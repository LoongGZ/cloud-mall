package com.self.cloudmall.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @version v1.0
 * @ClassName: CloudCartApplication
 * @Description:
 * @Author: M10003729
 */
@EnableRedisHttpSession
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CloudCartApplication {
    public static void main(String[] args) {
        SpringApplication.run(CloudCartApplication.class, args);
    }
}
