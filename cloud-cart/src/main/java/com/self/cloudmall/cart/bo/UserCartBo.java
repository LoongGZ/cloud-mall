package com.self.cloudmall.cart.bo;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: UserCartBo
 * @Description:
 * @Author: M10003729
 */
@Data
public class UserCartBo {
    private Long userId;
    private String userKey;
    private boolean isTempUser = false;
}
