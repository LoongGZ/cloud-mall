package com.self.cloudmall.cart.client;

import com.self.cloudmall.common.dto.CartItemCheckedDto;
import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("cloud-pms")
public interface SkuClient {
    @RequestMapping("pms/skuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);

    @GetMapping("pms/skuinfo/saleAttr/{skuId}")
    R getSaleAttrs(@PathVariable("skuId") Long skuId);

    @PostMapping("pms/skuinfo/getSkus")
    List<CartItemCheckedDto> getSkus(@RequestBody List<Long> skuIds);
}
