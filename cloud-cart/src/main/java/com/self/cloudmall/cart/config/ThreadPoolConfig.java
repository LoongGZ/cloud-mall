package com.self.cloudmall.cart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @version v1.0
 * @ClassName: ThreadPoolConfig
 * @Description:
 * @Author: M10003729
 */
@Configuration
public class ThreadPoolConfig {

    @Bean
    public ThreadPoolExecutor executorService(ThreadPoolProperties poolProp){
        return new ThreadPoolExecutor(poolProp.getCoreSize(),poolProp.getMaxSize(),
                poolProp.getAliveTime(), TimeUnit.SECONDS,new LinkedBlockingQueue<>(poolProp.getWorkQueueSize()));
    }
}
