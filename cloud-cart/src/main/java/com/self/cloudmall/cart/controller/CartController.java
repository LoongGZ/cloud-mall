package com.self.cloudmall.cart.controller;

import com.self.cloudmall.cart.service.CartService;
import com.self.cloudmall.cart.vo.CartItemVo;
import com.self.cloudmall.cart.vo.CartVo;
import com.self.cloudmall.common.enumm.ResultErrorEum;
import com.self.cloudmall.common.utils.DomainUtil;
import com.self.cloudmall.common.utils.R;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @version v1.0
 * @ClassName: CartController
 * @Description:
 * @Author: M10003729
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/cart/getCurrentCartItems")
    @ResponseBody
    public R getCurrentCartItems(){
       return cartService.getCurrentCartItems();
    }

    @GetMapping("/delCartItem")
    public String delCartItem(Long skuId,HttpServletRequest request){
        cartService.delCartItem(skuId);
        return "redirect:"+ "http://cart.cloudmall.com/cart.html";
    }

    @GetMapping("/checkCartItem")
    public String checkCartItem(Long skuId, Integer isChecked, HttpServletRequest request){
        cartService.checkCartItem(skuId,isChecked);
        return "redirect:"+ "http://cart.cloudmall.com/cart.html";
    }

    @GetMapping("/modifyItemNum")
    public String modifyItemNum(Long skuId, Integer num, HttpServletRequest request){
        if (skuId == null || num == null || num < 0){
            return "redirect:"+ "http://cart.cloudmall.com/cart.html";
        }
        cartService.modifyItemNum(skuId,num);
        return "redirect:"+ "http://cart.cloudmall.com/cart.html";
    }


    @GetMapping("/cart.html")
    public String cartPage(Model model){
        CartVo cartVo = cartService.getCart();
        model.addAttribute("cart",cartVo);
        return "cartList";
    }

    @GetMapping("/addToCart")
    public String addToCart(Long skuId, Integer num, RedirectAttributes redirect, HttpServletRequest request){
        if (skuId == null || num == null || num < 0){
            redirect.addAttribute("msg", ResultErrorEum.PARAM_EMPTY.getCode());
        }
        CartItemVo itemVo = cartService.addToCart(skuId,num);
        if (itemVo == null){
            redirect.addAttribute("msg", ResultErrorEum.CART_ADD_FAIL.getCode());
        }else{
            redirect.addAttribute("msg","0");
        }
        redirect.addAttribute("skuId",skuId);
        return "redirect:"+"http://cart.cloudmall.com/cartSuccess.html";
    }

    @GetMapping("/cartSuccess.html")
    public String cartSuccess(Long skuId,String msg, Model model){
        if (StringUtils.isNotEmpty(msg)){
           model.addAttribute("msg",ResultErrorEum.getMsg(Integer.parseInt(msg)));
            return "success";
        }
        CartItemVo itemVo = cartService.getCartItem(skuId);
        model.addAttribute("item",itemVo);
        model.addAttribute("msg",msg);
        return "success";
    }
}
