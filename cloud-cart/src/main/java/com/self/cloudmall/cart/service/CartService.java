package com.self.cloudmall.cart.service;

import com.self.cloudmall.cart.vo.CartItemVo;
import com.self.cloudmall.cart.vo.CartVo;
import com.self.cloudmall.common.utils.R;

/**
 * @version v1.0
 * @ClassName: CartService
 * @Description:
 * @Author: M10003729
 */
public interface CartService {
    CartItemVo addToCart(Long skuId, Integer num);

    CartItemVo getCartItem(Long skuId);

    CartVo getCart();

    void checkCartItem(Long skuId, Integer isChecked);

    void modifyItemNum(Long skuId, Integer num);

    void delCartItem(Long skuId);

    R getCurrentCartItems();
}
