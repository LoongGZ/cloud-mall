package com.self.cloudmall.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version v1.0
 * @ClassName: CartItemVo
 * @Description:
 * @Author: M10003729
 */
public class CartItemVo {
    private Long skuId;
    private String skuTitle;
    private String image;
    private BigDecimal price;
    private Integer count;
    private BigDecimal totalPrice;
    private Boolean checked = true;
    private List<String> skuAttrs;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getSkuTitle() {
        return skuTitle;
    }

    public void setSkuTitle(String skuTitle) {
        this.skuTitle = skuTitle;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCount() {
        if (this.count == null){
            return 0;
        }
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getTotalPrice() {
        if (this.price != null && (this.price.compareTo(BigDecimal.ZERO) > 0)
        && this.count != null && this.count > 0){
            return this.price.multiply(new BigDecimal(""+this.count));
        }
        return BigDecimal.ZERO;
    }


    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public List<String> getSkuAttrs() {
        return skuAttrs;
    }

    public void setSkuAttrs(List<String> skuAttrs) {
        this.skuAttrs = skuAttrs;
    }
}
