package com.self.cloudmall.cart.vo;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version v1.0
 * @ClassName: CartVo
 * @Description:
 * @Author: M10003729
 */
public class CartVo {
    private List<CartItemVo> items;
    private Integer countNum;//商品数量
    private Integer countTypeNum;//商品类型数量
    private BigDecimal totalAmount;//商品总金额
    private BigDecimal reduce = new BigDecimal("0");//减免金额

    public List<CartItemVo> getItems() {
        return items;
    }

    public void setItems(List<CartItemVo> items) {
        this.items = items;
    }

    public Integer getCountNum() {
        Integer x = 0;
        if (!CollectionUtils.isEmpty(this.items)){
           for (CartItemVo item : items){
               x += item.getCount();
           }
        }
        return x;
    }


    public Integer getCountTypeNum() {
        if (!CollectionUtils.isEmpty(this.items)){
            return this.items.size();
        }
        return 0;
    }


    public BigDecimal getTotalAmount() {
        BigDecimal total = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(this.items)){
            for (CartItemVo item : items){
                if (item.getChecked()){
                    total = total.add(item.getTotalPrice());
                }
            }
            total = total.subtract(getReduce());
        }
        if (total.compareTo(BigDecimal.ZERO) < 0){
            return BigDecimal.ZERO;
        }
        return total;
    }


    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
