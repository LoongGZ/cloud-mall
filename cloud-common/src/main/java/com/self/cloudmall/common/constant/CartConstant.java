package com.self.cloudmall.common.constant;

/**
 * @version v1.0
 * @InterfaceName: CartConstant
 * @Description:
 * @Author: M10003729
 */
public interface CartConstant {
    String TEMP_USER_KEY_NAME = "user-key";
    int TEMP_USER_KEY_EXPIRE = 60*60*24*30;
    String CART_REDIS_PREFIX = "cart:";
    String SPACE = ":";

}
