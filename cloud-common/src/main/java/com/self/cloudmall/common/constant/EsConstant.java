package com.self.cloudmall.common.constant;

public interface EsConstant {
    String PRODUCT_INDEX = "cloud_product";
    int ES_PAGE_SIZE = 2;
}
