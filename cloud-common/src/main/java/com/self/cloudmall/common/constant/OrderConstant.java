package com.self.cloudmall.common.constant;

public interface OrderConstant {
    String ORDER_TOKEN_PREFIX="order:token:";
    int ORDER_TOKEN_EXPIRE = 1800;
    int ORDER_AUTO_CONFIRM_TIME = 7;
}
