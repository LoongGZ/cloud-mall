package com.self.cloudmall.common.constant;

/**
 * @version v1.0
 * @InterfaceName: PmsConstant
 * @Description:
 * @Author: M10003729
 * @Date: 2020/9/8 11:16
 */
public interface PmsConstant {
    int ENABLE = 1;
    int DISABLE = 0;
    int DEFAULT = 1;
    int UNDEFAULT = 0;
    long CATEGORY_ROOT = 0L;
    String CACHE_PREFIX = "pms:";
    String SPACE = ":";
    String CACHE_CATEGORY = "categoryJson";
    long CACHE_CATEGORY_EXPIRE = 86400L;
}
