package com.self.cloudmall.common.constant;

/**
 * @version v1.0
 * @InterfaceName: SeckillConstant
 * @Description:
 * @Author: M10003729
 */
public interface SeckillConstant {
    String PROMOTIONSESSION_PREFIX = "seckill:promotionSession:";
    String SESSION_SKU_PREFIX = "seckill:sessionSkus";
    String SEMAPHORE_SKU_STOCK = "seckill:skuStock:";
    String UPLOAD_SKU_LOCK = "seckill:uploadSku:lock";
    String SECKILL_USER_PREFIX = "seckill:user:";
}
