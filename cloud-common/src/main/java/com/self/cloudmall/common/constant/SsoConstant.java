package com.self.cloudmall.common.constant;


public interface SsoConstant {
    int VALID_CODE_RESEND = 120000;
    int REDIS_VALID_EXPIRE = 10;
    String REDIS_VALID_CODE_KEY = "sms:code:";
    String LOGIN_USER = "loginUser";
    String ROOT_DOMAIN = "cloudmall.com";
}
