package com.self.cloudmall.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AttrsDto implements Serializable {
    private static final long serialVersionUID = 6671817934072406731L;
    private Long attrId;
    private String attrName;
    private String attrValue;
}
