package com.self.cloudmall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CartItemCheckedDto {
    private Long skuId;
    private BigDecimal price;
}
