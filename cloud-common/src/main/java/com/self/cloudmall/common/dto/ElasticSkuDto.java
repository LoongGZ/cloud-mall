package com.self.cloudmall.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ElasticSkuDto implements Serializable {
   private static final long serialVersionUID = 831170200106880206L;
   private Long spuId;
   private Long skuId;
   private String skuTitle;
   private BigDecimal skuPrice;
   private String skuImg;
   private Long saleCount;
   private Boolean hasStock;
   private Long hotScore;
   private Long brandId;
   private String brandName;
   private String brandImg;
   private Long catalogId;
   private String catalogName;
   private List<AttrsDto> attrs;


}