package com.self.cloudmall.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @version v1.0
 * @ClassName: FullReductionDto
 * @Description:
 */
@Data
public class FullReductionDto implements Serializable {
    private static final long serialVersionUID = 6638198821350861729L;
    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPriceDto> memberPrice;
}
