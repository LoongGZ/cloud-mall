package com.self.cloudmall.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MemberPriceDto implements Serializable {

    private static final long serialVersionUID = -3560580875983370885L;
    private Long id;
    private String name;
    private BigDecimal price;

}