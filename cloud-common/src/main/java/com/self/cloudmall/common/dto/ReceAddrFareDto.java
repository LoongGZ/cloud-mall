package com.self.cloudmall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReceAddrFareDto {
    private ReceAddressDto receAddress;
    private BigDecimal fare;
}
