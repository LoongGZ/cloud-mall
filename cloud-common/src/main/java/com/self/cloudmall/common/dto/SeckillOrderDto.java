package com.self.cloudmall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SeckillOrderDto {

    private String orderSn;
    /**
     * 活动场次id
     */
    private Long promotionSessionId;
    /**
     * 商品id
     */
    private Long skuId;
    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    private Long memberId;

    private String memberUsername;

    private Integer num;
    //收货地址
    private Long addrId;

}
