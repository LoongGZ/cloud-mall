package com.self.cloudmall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @version v1.0
 * @ClassName: SeckillSkuDto
 * @Description:
 * @Author: M10003729
 */
@Data
public class SeckillSkuDto{
    /**
     * 活动id
     */
    private Long promotionId;
    /**
     * 活动场次id
     */
    private Long promotionSessionId;
    /**
     * 商品id
     */
    private Long skuId;
    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;
    /**
     * 秒杀总量
     */
    private Integer seckillCount;
    /**
     * 每人限购数量
     */
    private Integer seckillLimit;
    /**
     * 排序
     */
    private Integer seckillSort;

    private SkuInfoDto skuInfoDto;

    /**
     * 每日开始时间
     */
    private Long startTime;
    /**
     * 每日结束时间
     */
    private Long endTime;
    /**
     * 随机码，用于安全，防止提前秒杀
     */
    private String randomCode;
}
