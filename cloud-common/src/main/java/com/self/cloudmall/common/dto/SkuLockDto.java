package com.self.cloudmall.common.dto;

import lombok.Data;

@Data
public class SkuLockDto {
    private Long skuId;
    private String skuTitle;
    private Integer count;
}
