package com.self.cloudmall.common.dto;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: SocialLoginDto
 * @Description:
 * @Author: M10003729
 */
@Data
public class SocialLoginDto {
    private String accessToken;
    private Integer expiresIn;
    private String uid;
    private String userInfoUrl;

}
