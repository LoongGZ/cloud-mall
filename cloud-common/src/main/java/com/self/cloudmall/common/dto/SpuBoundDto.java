package com.self.cloudmall.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version v1.0
 * @ClassName: SpuBoundDto
 * @Description:
 */
@Data
public class SpuBoundDto implements Serializable {
    private static final long serialVersionUID = -1462879884427197844L;
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
