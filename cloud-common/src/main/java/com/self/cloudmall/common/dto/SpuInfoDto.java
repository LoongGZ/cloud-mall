package com.self.cloudmall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpuInfoDto {
    private Long id;
    /**
     * 商品名称
     */
    private String spuName;
    /**
     * 商品描述
     */
    private String spuDescription;
    /**
     * 所属分类id
     */
    private Long catalogId;
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     *
     */
    private BigDecimal weight;
}
