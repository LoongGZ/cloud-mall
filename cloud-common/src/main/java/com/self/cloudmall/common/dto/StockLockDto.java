package com.self.cloudmall.common.dto;

import lombok.Data;

@Data
public class StockLockDto {
    private Long id;
    /**
     * order_sn
     */
    private String orderSn;

    /**
     * sku_id
     */
    private Long skuId;
    /**
     * sku_name
     */
    private String skuName;
    /**
     * 购买个数
     */
    private Integer skuNum;

    private Long wareId;

    /**
     * 1:锁定，2：解锁，3：扣减
     */
    private Integer lockState;
}
