package com.self.cloudmall.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class WareSkuLockDto {
    private String orderSn;
    private List<SkuLockDto> skuLocks;
}
