package com.self.cloudmall.common.enumm;

/**
 * @version v1.0
 * @EnumName: AttrTypeEnum
 * @Description:
 */
public enum AttrTypeEnum {
    ATTR_TYPE_SALE(0,"sale","销售属性"),
    ATTR_TYPE_BASE(1,"base","基本属性");
    private int code;
    private String type;
    private String typeName;

    AttrTypeEnum(int code, String type, String typeName) {
        this.code = code;
        this.type = type;
        this.typeName = typeName;
    }

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }
}
