package com.self.cloudmall.common.enumm;

public enum OrderConfirmStateEnum {
    UNCONFIRM(0,"未确认"),
    CONFIRMED(1,"已确认");

    private int code;
    private String name;

    OrderConfirmStateEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
