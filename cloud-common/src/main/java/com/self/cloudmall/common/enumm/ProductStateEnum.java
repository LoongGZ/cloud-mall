package com.self.cloudmall.common.enumm;

public enum ProductStateEnum {
    //0 - 创建，1 - 上架，2-下架
    CREATE(0,"创建"),
    UP(1,"上架"),
    DOWN(2,"下架");

    private int code;
    private String name;

    ProductStateEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
