package com.self.cloudmall.common.enumm;

/**
 * @version v1.0
 * @EnumName: PurDetailStateEnum
 * @Description:
 */
public enum PurDetailStateEnum {
    //0新建，1已分配，2正在采购，3已完成，4采购失败
    STATENEW(0,"新建"),
    ASSIGN(1,"已分配"),
    PURCHASING(2,"正在采购"),
    FINISH(3,"已完成"),
    FAIL(4,"采购失败");
    private int code;
    private String name;

    PurDetailStateEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
