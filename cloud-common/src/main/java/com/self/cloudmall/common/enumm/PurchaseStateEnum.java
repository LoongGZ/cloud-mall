package com.self.cloudmall.common.enumm;

/**
 * @version v1.0
 * @EnumName: PurchaseStateEnum
 * @Description:
 */
public enum PurchaseStateEnum {
    STATENEW(0,"新建"),
    ASSIGN(1,"已分配"),
    RECEIVED(2,"已领取"),
    FINISH(3,"已完成"),
    EXCEPTION(4,"有异常");

    private int code;
    private String name;

    PurchaseStateEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
