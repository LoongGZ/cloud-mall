package com.self.cloudmall.common.enumm;

/**
 * @version v1.0
 * @ClassName: ResultErrorEum
 * @Description:
 * 系统错误 100*
 * 商品服务异常 200*
 * 用户服务异常 300*
 * 订单服务异常 400*
 * 营销服务异常 500*
 * 库存服务异常 600*
 * 购物车服务异常 700*
 *
 * @Author: M10003729
 */
public enum  ResultErrorEum {
    SYSTEM_ERROR(1000,"系统异常，请联系管理员"),
    PARAM_EMPTY(1001,"参数不能为空"),
    USERNAME_REPEAT(3001,"用户名重复"),
    MOBILE_REPEAT(3002,"手机号重复"),
    CART_ADD_FAIL(7001,"添加购物车失败，请重新添加"),
    ORDER_LOCK_STOCK_FAIL(6001,"锁定库存失败"),
    SENTINEL_BLOCK_ERROR(1002,"系统流量过大，请稍后重试"),
    ;
    private int code;
    private String message;

    ResultErrorEum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String getMsg(int code){
        for (ResultErrorEum errorEum : ResultErrorEum.values()){
            if (code == errorEum.getCode()){
                return errorEum.getMessage();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
