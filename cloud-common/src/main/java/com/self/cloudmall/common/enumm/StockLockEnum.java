package com.self.cloudmall.common.enumm;

public enum StockLockEnum {
    LOCKED(1,"锁定"),
    UNLOCK(2,"解锁"),
    LOCKFAIL(3,"扣减失败");
    private int code;
    private String name;

    StockLockEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
