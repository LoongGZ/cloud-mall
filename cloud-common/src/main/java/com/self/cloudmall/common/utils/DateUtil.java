package com.self.cloudmall.common.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @version v1.0
 * @ClassName: DateUtil
 * @Description:
 * @Author: M10003729
 */
public class DateUtil {

    /**
     * @Title: 获取当前时间的开始时间
     * @Description:
     * @Param []
     * @Return java.lang.String
     * @Author M10003729
     * @Throws
     */
    public static String getCurStartTime(){
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * @Title: 获取结束时间
     * @Description:
     * @Param []
     * @Return java.lang.String
     * @Author M10003729
     * @Throws
     */
    public static String getEndTime(Integer num){
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now().plusDays(num), LocalTime.MAX);
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
