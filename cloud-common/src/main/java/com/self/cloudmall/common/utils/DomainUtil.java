package com.self.cloudmall.common.utils;

import javax.servlet.http.HttpServletRequest;

public class DomainUtil {

    public static String getNoContextDomain(HttpServletRequest request){
        StringBuffer url = request.getRequestURL();
        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
        return tempContextUrl;
    }
}
