package com.self.cloudmall.common.utils;

public class ThreadLocalUtil{

    private static ThreadLocal<Object> threadLocal = new ThreadLocal<>();

    public static <T> void set(T t){
        threadLocal.set(t);
    }

    public static <T> T get(){
        return (T)threadLocal.get();
    }

    public static void remove(){
        threadLocal.remove();
    }


}
