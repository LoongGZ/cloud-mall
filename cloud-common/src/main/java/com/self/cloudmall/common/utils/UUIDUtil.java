package com.self.cloudmall.common.utils;

import java.util.UUID;

/**
 * @version v1.0
 * @ClassName: UUIDUtil
 * @Description:
 * @Author: M10003729
 */
public class UUIDUtil {

    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
