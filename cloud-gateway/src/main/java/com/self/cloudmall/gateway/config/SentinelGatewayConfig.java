package com.self.cloudmall.gateway.config;


import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.fastjson.JSONObject;
import com.self.cloudmall.common.enumm.ResultErrorEum;
import com.self.cloudmall.common.utils.R;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

@Configuration
public class SentinelGatewayConfig {

    @PostConstruct
    public void init(){
        //触发流控，返回的结果
        GatewayCallbackManager.setBlockHandler((ServerWebExchange exchange, Throwable e)->{
            R error = R.error(ResultErrorEum.SENTINEL_BLOCK_ERROR.getCode(), ResultErrorEum.SENTINEL_BLOCK_ERROR.getMessage());
            return ServerResponse.ok().body(Mono.just(JSONObject.toJSONString(error)),String.class);
        });
    }
}
