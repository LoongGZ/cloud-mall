package com.self.cloudmall.oms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("cloud-cart")
public interface CartProductClient {

    @GetMapping("/cart/getCurrentCartItems")
    R getCurrentCartItems();
}
