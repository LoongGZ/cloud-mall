package com.self.cloudmall.oms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @version v1.0
 * @InterfaceName: ProductAttr
 * @Description:
 * @Author: M10003729
 */
@FeignClient("cloud-pms")
public interface ProductAttrFeignClient {

    @GetMapping("/pms/productattrvalue/test/{id}")
    R test(@PathVariable("id") Long id);

    @GetMapping("/pms/spuinfo/{skuId}")
    R getSpuInfo(@PathVariable("skuId") Long skuId);
}
