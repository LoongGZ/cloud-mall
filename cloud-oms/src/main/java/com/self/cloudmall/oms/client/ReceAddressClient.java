package com.self.cloudmall.oms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("cloud-ums")
public interface ReceAddressClient {

    @GetMapping("/ums/memberreceiveaddress/{memberId}/getAddressList")
    R getAddressList(@PathVariable Long memberId);

    @GetMapping("/ums/memberreceiveaddress/info/{id}")
    R info(@PathVariable("id") Long id);
}
