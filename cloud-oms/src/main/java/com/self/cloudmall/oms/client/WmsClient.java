package com.self.cloudmall.oms.client;

import com.self.cloudmall.common.dto.WareSkuLockDto;
import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient("cloud-wms")
public interface WmsClient {
    @PostMapping("/wms/waresku/hasStock")
    R<Map<Long,Boolean>> hasStock(@RequestBody List<Long> skuIds);
    @GetMapping("/wms/wareinfo/fare")
    R getFare(@RequestParam("addrId") Long addrId);
    @PostMapping("/wms/waresku/order/lock")
    R orderLockStock(@RequestBody WareSkuLockDto skuLockDto);
}
