package com.self.cloudmall.oms.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.self.cloudmall.oms.vo.PayVo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Slf4j
@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000117604351";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC/rHySLt7F+J6KYQY3WsZKqXVKBhPsqx013gDdjd7E7Rn3R/pgEwWKIVvUiATEqLzlvIex6LXmZhJhNz+dVT3xq4HqVQ7fsSSBBRCrMpK6UUakUPYhqApS1gRjYzQGUosaRjy4+qqsRZOlL7jdKsjGHbo+TFdZlsUp8qVf8/vcdIuhn6i/5LldF+bg/MrbPVGaz8pZtjXD8JuVmKQbV6J91EbCeJYLClUPTfXaAs8qa/8fDlHGR6uiIo0Y260GYFPLthtlZRvwEwPN+/16xtW/1LoYuTYp23aPtOGnXE46YDkFlNyYfP34AHg6gnHRvZY+ppc/acL6gSgtYlEEdPcFAgMBAAECggEAaCW3Ak9JaNSwgIw7rnQ6bnMpcfFffid0FcLtQXVG6EXU6BJKL03moqmBljfaL2loSoRoTvkdbXVk40H75OdeYyRH0SYqkM1WAmzYI821AFwNQFdlheAiy+PSOpa6u/y57Krhb7l0Z2re0NDfZ/1JXqzkd9lPVTShKSvZRAv0fFI8QtD0Q1ngkDtJGBed8aD9wDmJtui524xnwWyW0t81g8+pghPs87EPj3ICpU0qgE35hg5bw3VwP2LlWjeJxCw6Foj3Shu2Zi+Pjp6RwMR/PXi5l9MFkCdu32C65p9ekacL2Gzyvka2J1Rwcu1KgDhjLjeBXxM0GekzfAJ+Vpo4gQKBgQD+p/PbvtQst5GNrbaX+p0XfvoBmTOqX2sqWU/6ztPeOYnEi3pOlKTXP8ITbs7W8YviOkh32G+chHyipL6+dEIXlWPzIad4Id5uALBENxe/4ih9moWLT4yGUTQuTX6imO7/H2PhkrKcYkIlwSYWCUwdw3WWVaP9CY5XVyev3cLtNQKBgQDAr3F2KLB58KaeE0cwyJ9Prmx68xL672HGipHJNR8ahv8WLjZf6GY+lkvffZgDVxSFmAnU6ZgFiL3SNEwQJTvSvoQX4V+wwsLFWUALzH0X1f7FzHxLhK4oDbxH7GPWU24cGPCdxcifvZv9N5kF+otMPF577VHdI6Z5F/rNmZmskQKBgQCXU/Uc84SgYiGzwLQOElaB3OnyQzkpmHRVgSd/EqNinmyZCI2Q3tIqO3A4HhP6NSX8NMhVt59zsmNiP2Y+RVX1L8hqdehQAkhstCoLD3Ykh9+p7EZJp+cI9n58uX50X08++kdfh85uEhHe362jULkTwtBAmvnNZ31Xt6oU30JztQKBgQC06EPT11IRuuMEiT8Oy13f9vi82tlYJkabnOBRlAF264Y18f91PoI4gu4LkCSYBjsrTtr0PrlPgNoPsLA1fG/VZrAop54rHEC3/xrFuSE9rto8JcgRMyo8SuEaehQmV0CLOtM2nkrFx8TcEkYmudvtIMTo/6Ebyevzx5LfVDZXMQKBgQC48Sr/Ja9v3DSOkmm/kq9oFjiffOD92WOmREEGNH+AudolBBd+j1//AfUVvrmYARJkyf9m5eQ+6Z2tLaUspMs/aeE4GSgXKBZHLbhxXsxRJ/7iY8FJJ59mJRsKUkW4pCTXAaDk6lSnMiK9trbrZyoZoerXiDVrZV+KKGB2wp6Bxw==";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwtoHQYkEp1CR2+t6sfxYTBvLYUPnIl+FKj/X+PXaKQdeM7nD91OywqXiDXTwto46o1NNDYc5bJDDqlpIOBHn/SHgmymfIER9bt11ZdyWa00JH20SCQKykJ7bFNB9llLwCBJX0iDan1m1MWOgIo3w7dPgfeO62aFv2WAXcsZc+9wElxbzuHzf/5CEasFP8jbW+tl+gBUNI+GTqGDGvR/QnuFesUvsUPRmYKGqD8FsKqXQcFAhhfKrSknQj8C02N+S8Hlc+vojDAAyMNMrTH87jDhwG1ANCGfQspA3+PnfObhE90xI1Y7hHHnw4iuzl1gSE7pPnGrmhQkSNUahwV3z/QIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url="http:///order.cloudmall.com/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.cloudmall.com/home.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付超时时间默认30分钟,防止库存解锁后，支付宝还能支付
    private  String pay_timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ vo.getOutTradeNo() +"\","
                + "\"total_amount\":\""+ vo.getTotalAmount() +"\","
                + "\"subject\":\""+ vo.getSubject() +"\","
                + "\"body\":\""+ vo.getBody() +"\","
                + "\"timeout_express\":\""+ this.getPay_timeout() +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        log.info("支付宝的响应："+result);

        return result;

    }
}
