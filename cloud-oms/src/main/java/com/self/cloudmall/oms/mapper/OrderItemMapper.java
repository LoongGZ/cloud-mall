package com.self.cloudmall.oms.mapper;

import com.self.cloudmall.oms.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItemEntity> {
	
}
