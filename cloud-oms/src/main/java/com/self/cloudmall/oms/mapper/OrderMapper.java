package com.self.cloudmall.oms.mapper;

import com.self.cloudmall.oms.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
	
}
