package com.self.cloudmall.oms.mapper;

import com.self.cloudmall.oms.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
@Mapper
public interface OrderSettingMapper extends BaseMapper<OrderSettingEntity> {
	
}
