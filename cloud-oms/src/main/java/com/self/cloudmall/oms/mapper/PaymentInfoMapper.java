package com.self.cloudmall.oms.mapper;

import com.self.cloudmall.oms.entity.PaymentInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付信息表
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
@Mapper
public interface PaymentInfoMapper extends BaseMapper<PaymentInfoEntity> {
	
}
