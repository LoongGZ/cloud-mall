package com.self.cloudmall.oms.mq;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.self.cloudmall.oms.entity.OrderEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
//@RabbitListener(queues = "my.test.que")
@Slf4j
public class MsgTest {

   // @RabbitHandler
    public void consum(Message message, OrderEntity orderEntity, Channel channel){
        log.info("消息={},获取消息对象={}", JSONObject.toJSONString(message),JSONObject.toJSONString(orderEntity));
    }
}
