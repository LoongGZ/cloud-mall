package com.self.cloudmall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.oms.utils.PageUtils;
import com.self.cloudmall.oms.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

