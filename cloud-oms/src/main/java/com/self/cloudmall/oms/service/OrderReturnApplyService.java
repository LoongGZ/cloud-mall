package com.self.cloudmall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.oms.utils.PageUtils;
import com.self.cloudmall.oms.entity.OrderReturnApplyEntity;

import java.util.Map;

/**
 * 订单退货申请
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
public interface OrderReturnApplyService extends IService<OrderReturnApplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

