package com.self.cloudmall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.dto.OrderDto;
import com.self.cloudmall.common.dto.SeckillOrderDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.oms.utils.PageUtils;
import com.self.cloudmall.oms.entity.OrderEntity;
import com.self.cloudmall.oms.vo.OrderConfirmVo;
import com.self.cloudmall.oms.vo.PayAsyncVo;
import com.self.cloudmall.oms.vo.SubmitOrderVo;

import java.util.Map;

/**
 * 订单
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    OrderConfirmVo getOrderConfirm();

    R submitOrder(SubmitOrderVo submitOrder) throws Exception;

    void orderClose(OrderDto order) throws Exception;

    R pay(String orderSn);

    PageUtils orderList(Map<String, Object> params);

    R handlePayed(PayAsyncVo payAsyncVo);

    void createSeckillOrder(SeckillOrderDto seckillOrder)throws Exception;
}

