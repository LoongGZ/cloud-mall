package com.self.cloudmall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.oms.utils.PageUtils;
import com.self.cloudmall.oms.entity.OrderSettingEntity;

import java.util.Map;

/**
 * 订单配置信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
public interface OrderSettingService extends IService<OrderSettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

