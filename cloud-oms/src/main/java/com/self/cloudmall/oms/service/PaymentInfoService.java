package com.self.cloudmall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.oms.utils.PageUtils;
import com.self.cloudmall.oms.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-11 22:28:11
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

