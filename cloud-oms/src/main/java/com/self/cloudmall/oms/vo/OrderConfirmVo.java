package com.self.cloudmall.oms.vo;


import com.self.cloudmall.common.dto.ReceAddressDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class OrderConfirmVo {
    //收货人地址
    @Setter
    @Getter
    private List<ReceAddressDto> receAddressList;
    //商品项
    @Setter
    @Getter
    private List<OrderItemVo> orderItemList;
    //优惠券，积分等
    @Setter
    @Getter
    private Integer integration;

    //总金额
    private BigDecimal totalAmount;
    //优惠金额
    private BigDecimal discount;
    //应付金额
    private BigDecimal payAmount;
    //是否有库存
    @Setter
    @Getter
    private Map<Long, Boolean> hasStocks;
    //订单令牌，防止重复
    @Setter
    @Getter
    private String orderToken;

    public Integer getCount(){
        if (!CollectionUtils.isEmpty(orderItemList)){
            int count = 0;
            for (OrderItemVo itemVo : orderItemList){
                count += itemVo.getCount();
            }
            return count;
        }
        return 0;
    }

    public BigDecimal getTotalAmount() {
        if (!CollectionUtils.isEmpty(orderItemList)){
            BigDecimal total = BigDecimal.ZERO;
            for (OrderItemVo item : orderItemList){
                total = total.add(item.getPrice().multiply(new BigDecimal(item.getCount()+"")));
            }
            return total;
        }
        return BigDecimal.ZERO;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDiscount() {

        return BigDecimal.ZERO;
    }

    public BigDecimal getPayAmount() {
        BigDecimal subtract = getTotalAmount().subtract(getDiscount());
        if (subtract.compareTo(BigDecimal.ZERO) < 0){
            return BigDecimal.ZERO;
        }
        return subtract;
    }
}
