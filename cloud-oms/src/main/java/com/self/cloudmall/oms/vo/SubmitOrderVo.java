package com.self.cloudmall.oms.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SubmitOrderVo {
    private Long addrId;
    private Integer payType; //支付方式
    private String orderToken;//订单令牌，防重复提交
    private BigDecimal payAmount;//应付金额，校验
    private String note;//备注
    private Integer sourceType;//订单来源[0->PC订单；1->app订单]
}
