package com.self.cloudmall.oms;

import com.self.cloudmall.common.utils.UUIDUtil;
import com.self.cloudmall.oms.entity.OrderEntity;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;


/**
 * amqpAmin: 管理exchange，binding，queue
 * rabbitmqTemplate：发送和接受消息
 * @rabbitListener:  监听指定队列，可以放在类和方法上
 * @RabbitHandler:  监听队列处理，放在方法上，可以与@rabbitListener结合使用，适合于同一个消息处理不同类型的数据，相当于重载
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class CloudOmsApplicationTests {

    @Autowired
    private AmqpAdmin amqpAdmin;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 创建direct类型的exchange
     */
    @Test
    void createExchange() {
        DirectExchange exchange = new DirectExchange("my.test.ex",true,false);
        amqpAdmin.declareExchange(exchange);
        System.out.println("DirectExchange 创建成功=="+"my.test.ex");
    }
    @Test
    void createQueue(){
        Queue queue = new Queue("my.test.que",true,false,false);
        amqpAdmin.declareQueue(queue);
        System.out.println("Queue 创建成功=="+"my.test.que");
    }
    @Test
    void createBinding(){
        Binding binding = new Binding("my.test.que", Binding.DestinationType.QUEUE,"my.test.ex","my.test",null);
        amqpAdmin.declareBinding(binding);
        System.out.println("Binding 成功=="+"my.test.que-->"+"my.test.ex");
    }

    @Test
    void send(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(1L);
        orderEntity.setDeliverySn(UUIDUtil.generateUUID());
        orderEntity.setTotalAmount(new BigDecimal("1.11"));
        orderEntity.setBillContent("厦控股京哈高速科技爱国");
        orderEntity.setCreateTime(new Date());
        rabbitTemplate.convertAndSend("my.test.ex","my.test",orderEntity);
        System.out.println("发送成功 ==对象ID="+orderEntity.getId());
    }

}
