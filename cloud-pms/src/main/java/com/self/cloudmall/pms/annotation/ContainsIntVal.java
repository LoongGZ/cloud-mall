package com.self.cloudmall.pms.annotation;

import com.self.cloudmall.pms.validated.IntValConstraintValidator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @version v1.0
 * @AnnotationName:
 * @Description:
 */
@Constraint(validatedBy = {
        IntValConstraintValidator.class
})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContainsIntVal {
    int[] vals() default {};
}
