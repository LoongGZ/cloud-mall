package com.self.cloudmall.pms.client;

import com.self.cloudmall.common.dto.FullReductionDto;
import com.self.cloudmall.common.dto.SpuBoundDto;
import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: CouponClient
 * @Description:
 */
@FeignClient(value = "cloud-sms")
public interface CouponClient {
    /**
     * @Title: 积分保存，
     * feign支持dto传输对象类型不一致，只要属性名一致，因为是通过json转成被调用对象的
     * @Description:
     * @Param [spuBoundDto]
     * @Return com.self.cloudmall.common.utils.R
     */
    @PostMapping("/sms/spubounds/save")
    R save(@RequestBody SpuBoundDto spuBoundDto);

    @PostMapping("/sms/skufullreduction/batchAdd")
    R batchAdd(@RequestBody List<FullReductionDto> dtos);
}
