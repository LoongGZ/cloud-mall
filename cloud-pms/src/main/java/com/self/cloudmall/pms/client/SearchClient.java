package com.self.cloudmall.pms.client;

import com.self.cloudmall.common.dto.ElasticSkuDto;
import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("cloud-search")
public interface SearchClient {

    @PostMapping("/search/product/add")
    R productAdd(@RequestBody List<ElasticSkuDto> skuDtoList);
}
