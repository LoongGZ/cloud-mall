package com.self.cloudmall.pms.client;


import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("cloud-seckill")
public interface SeckillClient {

    @GetMapping("/seckill/{skuId}")
    R getSeckillSkuBySkuId(@PathVariable("skuId") Long skuId);
}
