package com.self.cloudmall.pms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient("cloud-wms")
public interface WareStockClient {

    @PostMapping("/wms/waresku/hasStock")
    R<Map<Long,Boolean>> hasStock(@RequestBody List<Long> skuIds);
}
