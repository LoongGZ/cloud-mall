package com.self.cloudmall.pms.config;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor pagination = new PaginationInterceptor();
        //请求页超过最大页，true:跳转回第一页；false:继续跳转不存在的页，默认是false
        pagination.setOverflow(true);
        //设置最大limit,m默认是500；-1：无限制
        //pagination.setLimit(500);
        return pagination;
    }
}
