package com.self.cloudmall.pms.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @version v1.0
 * @ClassName: OSSConfig
 * @Description:
 * @Author: M10003729
 * @Date: 2020/9/1 18:03
 */
@Configuration
public class OSSConfig {

    @Value("${alibaba.cloud.access-key}")
    private String accessKey;
    @Value("${alibaba.cloud.secret-key}")
    private String secretKey;
    @Value("${alibaba.cloud.endpoint}")
    private String endpoint;


    @Bean
    public OSS ossClient(){
        return new OSSClientBuilder().build(endpoint, accessKey, secretKey);
    }
}
