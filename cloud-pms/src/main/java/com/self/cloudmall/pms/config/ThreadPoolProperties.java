package com.self.cloudmall.pms.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @ClassName: ThreadPoolProperties
 * @Description:
 * @Author: M10003729
 */
@ConfigurationProperties(prefix = "threadpool")
@Component
@Data
public class ThreadPoolProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer aliveTime;
    private Integer workQueueSize = 10000;
}
