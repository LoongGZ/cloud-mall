package com.self.cloudmall.pms.controller;

import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.ProductAttrValueEntity;
import com.self.cloudmall.pms.service.AttrService;
import com.self.cloudmall.pms.service.ProductAttrValueService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 商品属性
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@RestController
@RequestMapping("pms/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;
    @Autowired
    private ProductAttrValueService productAttrValueService;

    @PostMapping("/update/{spuId}")
    public R updateSpuAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> entities){
        productAttrValueService.updateSpuAttr(spuId,entities);
        return R.ok();
    }

    /**
     * 查询商品下的规格属性
     * @param spuId
     * @return
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrlistforspu(@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> entities = productAttrValueService.baseAttrlistforspu(spuId);
        return R.ok().put("data",entities);
    }

    /**
     * 查询分类下的商品属性规格或者销售属性
     * @param params
     * @param catelogId
     * @param type
     * @return
     */
    @GetMapping("/{type}/list/{catelogId}")
    // @RequiresPermissions("pms:attr:list")
    public R baseList(@RequestParam Map<String, Object> params,@PathVariable("catelogId") Long catelogId,
                      @PathVariable("type") String type){
        PageUtils page = attrService.queryPageByCatelogId(params,catelogId,type);
        return R.ok().put("page", page);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("pms:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
   // @RequiresPermissions("pms:attr:info")
    public R info(@PathVariable("attrId") Long attrId){
        AttrVo attr = attrService.getAttrVoById(attrId);
       // List<Long> catelogPaths = attrGroupService.findCatelogPath(attr.getCatelogId());

        return R.ok().put("attr", attr);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("pms:attr:save")
    public R save(@RequestBody AttrVo attrVo){
		//attrService.save(attr);
        attrService.saveAttr(attrVo);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("pms:attr:update")
    public R update(@RequestBody AttrVo attrVo){
		//attrService.updateById(attr);
        attrService.updateAttrEntity(attrVo);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("pms:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
