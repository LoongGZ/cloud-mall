package com.self.cloudmall.pms.controller;

import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.AttrEntity;
import com.self.cloudmall.pms.entity.AttrGroupEntity;
import com.self.cloudmall.pms.service.AttrGroupService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.AttrGroupRelationVo;
import com.self.cloudmall.pms.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 属性分组
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
@RestController
@RequestMapping("pms/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    /**
     * 列表
     */
    @RequestMapping("/list/{cId}")
   // @RequiresPermissions("pms:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,@PathVariable("cId") Long cId){
        PageUtils page = attrGroupService.queryPage(params,cId);

        return R.ok().put("page", page);
    }


    @GetMapping("/{attrgroupId}/attr/relation")
    // @RequiresPermissions("pms:attrgroup:list")
    public R relationList(@PathVariable("attrgroupId") Long attrgroupId){
        List<AttrEntity> relationList = attrGroupService.relationList(attrgroupId);
        return R.ok().put("data", relationList);
    }

    @GetMapping("/{attrgroupId}/noattr/relation")
    // @RequiresPermissions("pms:attrgroup:list")
    public R noRelationPage(@RequestParam Map<String, Object> params,
                          @PathVariable("attrgroupId") Long attrgroupId){
        PageUtils page = attrGroupService.noRelationPage(params,attrgroupId);
        return R.ok().put("page", page);
    }

    @GetMapping("/{catelogId}/withattr")
    // @RequiresPermissions("pms:attrgroup:list")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") Long catelogId){
        List<AttrGroupWithAttrsVo> data = attrGroupService.getAttrGroupWithAttrs(catelogId);
        return R.ok().put("data", data);
    }



    @PostMapping("/attr/relation/delete")
    // @RequiresPermissions("pms:attrgroup:list")
    public R relationDel(@RequestBody List<AttrGroupRelationVo> relationVos){
       attrGroupService.relationDel(relationVos);
        return R.ok();
    }

    @PostMapping("/attr/relation")
    //@RequiresPermissions("pms:attrgroup:save")
    public R addRelation(@RequestBody List<AttrGroupRelationVo> relationVos){
        attrGroupService.addRelation(relationVos);

        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
   // @RequiresPermissions("pms:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        List<Long> list = attrGroupService.findCatelogPath(attrGroup.getCatelogId());
        return R.ok().put("attrGroup", attrGroup).put("catelogPath",list);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("pms:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("pms:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("pms:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
