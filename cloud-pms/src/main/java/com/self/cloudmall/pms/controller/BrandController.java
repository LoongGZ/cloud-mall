package com.self.cloudmall.pms.controller;

import java.util.Arrays;
import java.util.Map;

import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.self.cloudmall.pms.entity.BrandEntity;
import com.self.cloudmall.pms.service.BrandService;



/**
 * 品牌
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 22:33:30
 */
@RestController
@RequestMapping("pms/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("pms:brand:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
   // @RequiresPermissions("pms:brand:info")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("pms:brand:save")
    public R save(@Validated @RequestBody BrandEntity brand){
        /*if (result.hasErrors()){
            FieldError fieldError = result.getFieldError();
            return R.error(400,fieldError.getDefaultMessage());
        }*/
		brandService.save(brand);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("pms:brand:update")
    public R update(@RequestBody BrandEntity brand){
		//brandService.updateById(brand);
        //关联更新品牌分类关联表的品牌名称
        brandService.updateRelative(brand);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("pms:brand:delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
