package com.self.cloudmall.pms.controller;

import com.self.cloudmall.common.dto.CartItemCheckedDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.SkuInfoEntity;
import com.self.cloudmall.pms.service.SkuInfoService;
import com.self.cloudmall.pms.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * sku信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@RestController
@RequestMapping("pms/skuinfo")
public class SkuInfoController {
    @Autowired
    private SkuInfoService skuInfoService;

    @PostMapping("/getSkus")
    public List<CartItemCheckedDto> getSkus(@RequestBody List<Long> skuIds){
        return skuInfoService.getSkus(skuIds);
    }


    @GetMapping("/saleAttr/{skuId}")
    public R getSaleAttrs(@PathVariable("skuId") Long skuId){
         return skuInfoService.getSaleAttrs(skuId);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("pms:skuinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = skuInfoService.queryPageByCondition(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{skuId}")
   // @RequiresPermissions("pms:skuinfo:info")
    public R info(@PathVariable("skuId") Long skuId){
		SkuInfoEntity skuInfo = skuInfoService.getById(skuId);

        return R.ok().put("skuInfo", skuInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("pms:skuinfo:save")
    public R save(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.save(skuInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("pms:skuinfo:update")
    public R update(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.updateById(skuInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("pms:skuinfo:delete")
    public R delete(@RequestBody Long[] skuIds){
		skuInfoService.removeByIds(Arrays.asList(skuIds));

        return R.ok();
    }

}
