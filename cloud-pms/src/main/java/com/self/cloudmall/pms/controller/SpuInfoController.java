package com.self.cloudmall.pms.controller;

import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.SpuInfoEntity;
import com.self.cloudmall.pms.service.SpuInfoService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.SpuSaveVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * spu信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 22:33:30
 */
@Slf4j
@RestController
@RequestMapping("pms/spuinfo")
public class SpuInfoController {
    @Autowired
    private SpuInfoService spuInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("pms:spuinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = spuInfoService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }

    @GetMapping("/{skuId}")
    public R getSpuInfo(@PathVariable("skuId") Long skuId){
        return spuInfoService.getSpuInfo(skuId);
    }

    /**
     * 商品上架
     * @param spuId
     * @return
     */
    @PostMapping("/{spuId}/up")
    public R spuUp(@PathVariable("spuId") Long spuId){
        return spuInfoService.spuUp(spuId);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("pms:spuinfo:info")
    public R info(@PathVariable("id") Long id){
		SpuInfoEntity spuInfo = spuInfoService.getById(id);

        return R.ok().put("spuInfo", spuInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("pms:spuinfo:save")
    public R save(@RequestBody SpuSaveVo spuInfo){
        try {
            spuInfoService.saveSpuSaveVo(spuInfo);
        } catch (Exception e) {
            log.error("spu保存失败：{}",e);
            return R.error("spu保存失败");
        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("pms:spuinfo:update")
    public R update(@RequestBody SpuInfoEntity spuInfo){
		spuInfoService.updateById(spuInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("pms:spuinfo:delete")
    public R delete(@RequestBody Long[] ids){
		spuInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
