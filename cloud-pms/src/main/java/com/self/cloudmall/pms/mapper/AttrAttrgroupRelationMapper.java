package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.self.cloudmall.pms.vo.AttrGroupRelationVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性&属性分组关联
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
@Mapper
public interface AttrAttrgroupRelationMapper extends BaseMapper<AttrAttrgroupRelationEntity> {
    boolean relationDel(@Param("relationVos") List<AttrGroupRelationVo> relationVos);
}
