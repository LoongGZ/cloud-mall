package com.self.cloudmall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.self.cloudmall.pms.entity.AttrGroupEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
@Mapper
public interface AttrGroupMapper extends BaseMapper<AttrGroupEntity> {
}
