package com.self.cloudmall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.self.cloudmall.pms.entity.AttrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 商品属性
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface AttrMapper extends BaseMapper<AttrEntity> {

    IPage<AttrEntity> noRelationPage(IPage<AttrEntity> page,@Param("params") Map<String, Object> params);
}
