package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:29
 */
@Mapper
public interface BrandMapper extends BaseMapper<BrandEntity> {
	
}
