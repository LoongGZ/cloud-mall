package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌分类关联
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface CategoryBrandRelationMapper extends BaseMapper<CategoryBrandRelationEntity> {
	
}
