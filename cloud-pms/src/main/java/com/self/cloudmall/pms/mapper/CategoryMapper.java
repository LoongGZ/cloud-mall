package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface CategoryMapper extends BaseMapper<CategoryEntity> {
	
}
