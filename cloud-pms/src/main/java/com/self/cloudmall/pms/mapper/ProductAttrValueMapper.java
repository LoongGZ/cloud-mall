package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * spu属性值
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface ProductAttrValueMapper extends BaseMapper<ProductAttrValueEntity> {

    List<ProductAttrValueEntity> queryEnableSearchAttrValueBySpuId(@Param("spuId") Long spuId);
}
