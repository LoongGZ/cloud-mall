package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface SkuImagesMapper extends BaseMapper<SkuImagesEntity> {
	
}
