package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.self.cloudmall.pms.vo.SkuItemSaleAttrVo;
import com.self.cloudmall.pms.vo.SpuBaseGroupAttrVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface SkuInfoMapper extends BaseMapper<SkuInfoEntity> {

    List<SpuBaseGroupAttrVo> querySpuBaseGroupAttr(@Param("spuId") Long spuId,@Param("catalogId") Long catalogId);

    List<SkuItemSaleAttrVo> querySkuItemSaleAttr(Long spuId);

    List<String> getSaleAttrs(Long skuId);
}
