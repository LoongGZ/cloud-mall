package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
@Mapper
public interface SpuImagesMapper extends BaseMapper<SpuImagesEntity> {
	
}
