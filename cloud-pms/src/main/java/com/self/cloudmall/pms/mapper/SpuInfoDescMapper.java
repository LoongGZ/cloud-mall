package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
@Mapper
public interface SpuInfoDescMapper extends BaseMapper<SpuInfoDescEntity> {
	
}
