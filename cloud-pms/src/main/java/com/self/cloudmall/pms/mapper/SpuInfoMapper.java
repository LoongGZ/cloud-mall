package com.self.cloudmall.pms.mapper;

import com.self.cloudmall.pms.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 22:33:30
 */
@Mapper
public interface SpuInfoMapper extends BaseMapper<SpuInfoEntity> {
	
}
