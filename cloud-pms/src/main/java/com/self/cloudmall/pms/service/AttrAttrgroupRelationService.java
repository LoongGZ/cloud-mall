package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.entity.AttrAttrgroupRelationEntity;
import com.self.cloudmall.pms.vo.AttrGroupRelationVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

