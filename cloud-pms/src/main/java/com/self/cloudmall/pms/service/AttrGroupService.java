package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.AttrEntity;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.entity.AttrGroupEntity;
import com.self.cloudmall.pms.vo.AttrGroupRelationVo;
import com.self.cloudmall.pms.vo.AttrGroupWithAttrsVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:27:22
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params,Long cId);

    List<Long> findCatelogPath(Long catelogId);

    List<AttrEntity> relationList(Long attrgroupId);

    boolean relationDel(List<AttrGroupRelationVo> relationVos);

    PageUtils noRelationPage(Map<String, Object> params, Long attrgroupId);

    boolean addRelation(List<AttrGroupRelationVo> relationVos);

    List<AttrGroupWithAttrsVo> getAttrGroupWithAttrs(Long catelogId);
}

