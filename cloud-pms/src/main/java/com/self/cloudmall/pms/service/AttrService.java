package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.AttrEntity;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean saveAttr(AttrVo attrVo);

    PageUtils queryPageByCatelogId(Map<String, Object> params, Long catelogId,String type);

    boolean updateAttrEntity(AttrVo attrVo);

    AttrVo getAttrVoById(Long attrId);
}

