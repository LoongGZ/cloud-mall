package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.BrandEntity;
import com.self.cloudmall.pms.utils.PageUtils;

import java.util.Map;

/**
 * 品牌
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:29
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean updateRelative(BrandEntity brand);
}

