package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.BrandEntity;
import com.self.cloudmall.pms.entity.CategoryBrandRelationEntity;
import com.self.cloudmall.pms.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean saveComplete(CategoryBrandRelationEntity categoryBrandRelation);

    List<BrandEntity> relationBrandlist(Long catId);
}

