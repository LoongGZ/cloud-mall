package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.entity.CategoryEntity;
import com.self.cloudmall.pms.vo.CategoryTwoVo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> queryAllWithTree();

    Integer logicRemoveByIds(List<Long> asList);

    boolean updateRelative(CategoryEntity category);

    List<CategoryEntity> queryOneLeveCategorys();

    Map<String, List<CategoryTwoVo>> getCategoryJson();
}

