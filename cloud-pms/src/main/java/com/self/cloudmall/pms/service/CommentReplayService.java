package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.CommentReplayEntity;
import com.self.cloudmall.pms.utils.PageUtils;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

