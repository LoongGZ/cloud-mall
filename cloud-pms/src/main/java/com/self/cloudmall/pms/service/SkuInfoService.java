package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.dto.CartItemCheckedDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.SkuInfoEntity;
import com.self.cloudmall.pms.entity.SpuInfoEntity;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.SkuItemVo;
import com.self.cloudmall.pms.vo.SkuVo;

import java.util.List;
import java.util.Map;

/**
 * sku信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean batchSaveSkuRelation(List<SkuVo> skus, SpuInfoEntity spuInfoEntity);

    PageUtils queryPageByCondition(Map<String, Object> params);

    List<SkuInfoEntity> querySkusBySpuId(Long spuId);

    SkuItemVo getItem(Long skuId);

    R getSaleAttrs(Long skuId);

    List<CartItemCheckedDto> getSkus(List<Long> skuIds);
}

