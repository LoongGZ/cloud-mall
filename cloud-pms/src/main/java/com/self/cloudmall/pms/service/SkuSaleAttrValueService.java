package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.SkuSaleAttrValueEntity;
import com.self.cloudmall.pms.utils.PageUtils;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

