package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.pms.entity.SpuInfoDescEntity;
import com.self.cloudmall.pms.utils.PageUtils;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 21:24:28
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

