package com.self.cloudmall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.pms.entity.SpuInfoEntity;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-09 22:33:30
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean saveSpuSaveVo(SpuSaveVo spuInfo) throws Exception;

    PageUtils queryPageByCondition(Map<String, Object> params);

    R spuUp(Long spuId);

    R getSpuInfo(Long skuId);
}

