package com.self.cloudmall.pms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.self.cloudmall.pms.entity.AttrAttrgroupRelationEntity;
import com.self.cloudmall.pms.entity.AttrEntity;
import com.self.cloudmall.pms.entity.AttrGroupEntity;
import com.self.cloudmall.pms.entity.CategoryEntity;
import com.self.cloudmall.pms.mapper.AttrAttrgroupRelationMapper;
import com.self.cloudmall.pms.mapper.AttrGroupMapper;
import com.self.cloudmall.pms.mapper.AttrMapper;
import com.self.cloudmall.pms.service.AttrAttrgroupRelationService;
import com.self.cloudmall.pms.service.AttrGroupService;
import com.self.cloudmall.pms.service.AttrService;
import com.self.cloudmall.pms.service.CategoryService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.utils.Query;
import com.self.cloudmall.pms.vo.AttrGroupRelationVo;
import com.self.cloudmall.pms.vo.AttrGroupWithAttrsVo;
import com.self.cloudmall.pms.vo.AttrVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupMapper, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;
    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationMapper attrAttrgroupRelationMapper;
    @Autowired
    private AttrMapper attrMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params,Long cId) {
        QueryWrapper<AttrGroupEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<AttrGroupEntity> wrapper = queryWrapper.lambda();
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)){
            wrapper.and(obj -> {
                obj.eq(AttrGroupEntity::getAttrGroupId,key)
                        .or().like(AttrGroupEntity::getAttrGroupName,key);
            });
        }
        if (cId == 0){
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
            return new PageUtils(page);
        }else{
            wrapper.eq(AttrGroupEntity::getCatelogId, cId);
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
            return new PageUtils(page);
        }
    }

    @Override
    public List<Long> findCatelogPath(Long catelogId) {
        List<Long> catelogPaths = Lists.newArrayList();
        if (catelogId != null){
            findCatelogParentId(catelogId,catelogPaths);
        }
        Collections.reverse(catelogPaths);
        return catelogPaths;
    }

    @Override
    public List<AttrEntity> relationList(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> list = attrAttrgroupRelationService.lambdaQuery()
                .eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId)
                .list();
        if (CollectionUtils.isEmpty(list)){
            return Lists.newArrayList();
        }
        List<Long> collect = list.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
        return attrService.listByIds(collect);
    }

    @Override
    public boolean relationDel(List<AttrGroupRelationVo> relationVos) {
        attrAttrgroupRelationMapper.relationDel(relationVos);
        return true;
    }

    @Override
    public PageUtils noRelationPage(Map<String, Object> params, Long attrgroupId) {
        //1.必须在该分类下的分组
        //2.去除已经引用的属性
        params.put("attrgroupId",attrgroupId);
        IPage<AttrEntity> page = attrMapper.noRelationPage(new Query<AttrEntity>().getPage(params),params);
        return new PageUtils(page);
    }

    @Override
    public boolean addRelation(List<AttrGroupRelationVo> relationVos) {
        List<AttrAttrgroupRelationEntity> collect = relationVos.stream().map(x -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(x.getAttrId());
            relationEntity.setAttrGroupId(x.getAttrGroupId());
            return relationEntity;
        }).collect(Collectors.toList());
        return attrAttrgroupRelationService.saveBatch(collect);
    }

    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrs(Long catelogId) {
        List<AttrGroupEntity> list = this.lambdaQuery().eq(AttrGroupEntity::getCatelogId, catelogId)
                .list();
        if (CollectionUtils.isEmpty(list)){
            return Lists.newArrayList();
        }
        List<AttrGroupWithAttrsVo> collect = list.stream().map(x -> {
            AttrGroupWithAttrsVo vo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(x, vo);
            List<AttrEntity> relationList = this.relationList(x.getAttrGroupId());
            List<AttrVo> attrVos = Lists.newArrayList();
            if (!CollectionUtils.isEmpty(relationList)) {
                relationList.stream().forEach(t -> {
                    AttrVo attrVo = new AttrVo();
                    BeanUtils.copyProperties(t, attrVo);
                    attrVos.add(attrVo);
                });
            }
            vo.setAttrs(attrVos);
            return vo;
        }).collect(Collectors.toList());
        return collect;
    }

    private void findCatelogParentId(Long catelogId,List<Long> catelogPaths){
        if (catelogId !=null && catelogId != 0){
            catelogPaths.add(catelogId);
            CategoryEntity categoryEntity = categoryService.getById(catelogId);
            findCatelogParentId(categoryEntity.getParentCid(),catelogPaths);
        }
    }

}