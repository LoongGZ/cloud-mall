package com.self.cloudmall.pms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.common.constant.PmsConstant;
import com.self.cloudmall.common.utils.Constant;
import com.self.cloudmall.pms.entity.CategoryBrandRelationEntity;
import com.self.cloudmall.pms.entity.CategoryEntity;
import com.self.cloudmall.pms.mapper.CategoryMapper;
import com.self.cloudmall.pms.service.CategoryBrandRelationService;
import com.self.cloudmall.pms.service.CategoryService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.utils.Query;
import com.self.cloudmall.pms.vo.CategoryTwoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * @Title: 这里使用的是jedis客户端，因为lettuce底层是netty通讯，底层判断了加入字节是否超出可用字段，
     * 如果大于，就抛出堆外内存溢出，lettuce默认是从jvm分配内存
     *
     */
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> queryAllWithTree() {
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);
        if (CollectionUtils.isEmpty(categoryEntities)){
            return null;
        }
        return categoryEntities.stream().filter(x -> x.getParentCid() == Constant.NO.longValue())
                .map(x -> {
                    x.setChildren(recursionChildren(x,categoryEntities));
                    return x;
                })
                .sorted((x,y) -> (x.getSort() == null?0:x.getSort()) - (y.getSort() == null?0:y.getSort()))
                .collect(Collectors.toList());
    }

    @Override
    public Integer logicRemoveByIds(List<Long> asList) {
        return this.baseMapper.deleteBatchIds(asList);
    }

    @Override
    public boolean updateRelative(CategoryEntity category) {
        //更新分类品牌关联表
        CategoryBrandRelationEntity relationEntity = new CategoryBrandRelationEntity();
        relationEntity.setCatelogName(category.getName());
        categoryBrandRelationService.lambdaUpdate().eq(CategoryBrandRelationEntity::getCatelogId,category.getCatId())
                .update(relationEntity);
        return this.updateById(category);
    }

    @Cacheable(value = "category",key = "#root.methodName")//,sync = true 同步是加锁
   /* @CachePut //更新缓存
    @CacheEvict //删除缓存
    @Caching //组合缓存注解使用*/
    @Override
    public List<CategoryEntity> queryOneLeveCategorys() {
        List<CategoryEntity> list = this.lambdaQuery().eq(CategoryEntity::getShowStatus, PmsConstant.ENABLE).list();
        if (CollectionUtils.isEmpty(list)){
            return Collections.EMPTY_LIST;
        }
        return list.stream().sorted((x,y) -> (x.getSort() == null?0:x.getSort()) - (y.getSort() == null?0:y.getSort()))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<CategoryTwoVo>> getCategoryJson() {
        Map<String, List<CategoryTwoVo>> map = (Map<String, List<CategoryTwoVo>>) redisTemplate.opsForValue().get(PmsConstant.CACHE_PREFIX + PmsConstant.CACHE_CATEGORY);
        if (CollectionUtils.isEmpty(map)){
            Map<String, List<CategoryTwoVo>> categoryMap = getCategoryMap();
            if (!CollectionUtils.isEmpty(categoryMap)){
                redisTemplate.opsForValue().set(PmsConstant.CACHE_PREFIX+PmsConstant.CACHE_CATEGORY,categoryMap,
                        PmsConstant.CACHE_CATEGORY_EXPIRE, TimeUnit.SECONDS);
            }
            return categoryMap;
        }else{
            return map;
        }
    }

    private Map<String, List<CategoryTwoVo>> getCategoryMap(){
        List<CategoryEntity> categoryEntities = this.lambdaQuery().eq(CategoryEntity::getShowStatus,PmsConstant.ENABLE).list();
        if (CollectionUtils.isEmpty(categoryEntities)){
            return Collections.emptyMap();
        }
        List<CategoryEntity> oneLevelList = getCategoryLevelList(categoryEntities,PmsConstant.CATEGORY_ROOT);
        if (CollectionUtils.isEmpty(oneLevelList)){
            return Collections.emptyMap();
        }
        return oneLevelList.stream().collect(Collectors.toMap(k -> String.valueOf(k.getCatId()),
                v -> {
                    List<CategoryEntity> entityList = getCategoryLevelList(categoryEntities,v.getCatId());
                    if (!CollectionUtils.isEmpty(entityList)){
                        return entityList.stream()
                                .map(x -> {
                                    CategoryTwoVo twoVo = new CategoryTwoVo(String.valueOf(x.getCatId()),x.getName(),String.valueOf(v.getCatId()),null);
                                    List<CategoryEntity> three = getCategoryLevelList(categoryEntities,x.getCatId());
                                    if (!CollectionUtils.isEmpty(three)){
                                        List<CategoryTwoVo.CategoryThreeVo> collect = three.stream()
                                                .map(t -> new CategoryTwoVo.CategoryThreeVo(String.valueOf(t.getCatId()), t.getName(), String.valueOf(x.getCatId())))
                                                .collect(Collectors.toList());
                                        twoVo.setCatalog3List(collect);
                                    }
                                    return twoVo;
                                }).collect(Collectors.toList());
                    }
                    return Collections.EMPTY_LIST;
                }));
    }

    private List<CategoryEntity> getCategoryLevelList(List<CategoryEntity> categoryEntities, long categoryRoot) {
        List<CategoryEntity> collect = categoryEntities.stream().filter(x -> x.getParentCid() == categoryRoot)
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)){
            return null;
        }
        return collect.stream()
                .sorted((z, y) -> (z.getSort() == null ? 0 : z.getSort()) - (y.getSort() == null ? 0 : y.getSort()))
                .collect(Collectors.toList());
    }

    private List<CategoryEntity> recursionChildren(CategoryEntity category,List<CategoryEntity> categoryEntities){
       return categoryEntities.stream().filter(x -> x.getParentCid() == category.getCatId())
                .map(x -> {
                    x.setChildren(recursionChildren(x,categoryEntities));
                    return x;
                })
                .sorted((x,y) -> (x.getSort() == null?0:x.getSort()) - (y.getSort() == null?0:y.getSort()))
                .collect(Collectors.toList());
    }

}