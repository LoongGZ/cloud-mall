package com.self.cloudmall.pms.service.impl;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.pms.entity.AttrEntity;
import com.self.cloudmall.pms.entity.ProductAttrValueEntity;
import com.self.cloudmall.pms.mapper.ProductAttrValueMapper;
import com.self.cloudmall.pms.service.ProductAttrValueService;
import com.self.cloudmall.pms.utils.PageUtils;
import com.self.cloudmall.pms.utils.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueMapper, ProductAttrValueEntity> implements ProductAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(
                new Query<ProductAttrValueEntity>().getPage(params),
                new QueryWrapper<ProductAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId) {
        return this.lambdaQuery().eq(ProductAttrValueEntity::getSpuId,spuId).list();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities) {
        //删除所有商品对应的属性规格，
        QueryWrapper<ProductAttrValueEntity> wrapper =  new QueryWrapper<>();
        wrapper.lambda().eq(ProductAttrValueEntity::getSpuId,spuId);
        this.baseMapper.delete(wrapper);
        //添加商品的属性规格
        List<ProductAttrValueEntity> collect = entities.stream().map(x -> {
            x.setSpuId(spuId);
            return x;
        }).collect(Collectors.toList());
        return this.saveBatch(collect);
    }

    @Override
    public List<ProductAttrValueEntity> queryAttrValueBySpuId(Long spuId) {
        return this.lambdaQuery().eq(ProductAttrValueEntity::getSpuId,spuId).list();
    }

    @Override
    public List<ProductAttrValueEntity> queryEnableSearchAttrValueBySpuId(Long spuId) {
        return this.baseMapper.queryEnableSearchAttrValueBySpuId(spuId);
    }

}