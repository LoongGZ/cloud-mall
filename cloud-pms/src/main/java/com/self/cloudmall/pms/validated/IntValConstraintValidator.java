package com.self.cloudmall.pms.validated;

import com.google.common.collect.Sets;
import com.self.cloudmall.pms.annotation.ContainsIntVal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

/**
 * @version v1.0
 * @ClassName: 自定义验证器
 * @Description: <ContainsIntVal,Integer> 一个是注解，一个是所要验证的数据类型
 */
public class IntValConstraintValidator implements ConstraintValidator<ContainsIntVal,Integer> {

    private Set<Integer> sets = Sets.newHashSet();
    @Override
    public void initialize(ContainsIntVal constraintAnnotation) {

        int[] vals = constraintAnnotation.vals();
        if (vals != null && vals.length > 0){
            for (int i : vals){
                sets.add(i);
            }
        }
    }

    @Override
    public boolean isValid(Integer val, ConstraintValidatorContext constraintValidatorContext) {
        return sets.contains(val);
    }
}
