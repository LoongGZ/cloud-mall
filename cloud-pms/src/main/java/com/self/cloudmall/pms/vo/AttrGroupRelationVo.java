package com.self.cloudmall.pms.vo;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: AttrGroupRelationVo
 * @Description:
 * @Author: M10003729
 * @Date: 2020/9/8 11:43
 */
@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
