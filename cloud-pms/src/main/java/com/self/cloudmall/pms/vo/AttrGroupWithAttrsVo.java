package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: AttrGroupWithAttrsVo
 * @Description:
 */
@Data
public class AttrGroupWithAttrsVo {
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    private List<AttrVo> attrs;
}
