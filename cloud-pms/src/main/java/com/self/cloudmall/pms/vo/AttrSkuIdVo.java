package com.self.cloudmall.pms.vo;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: AttrSkuIdVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class AttrSkuIdVo {
    //多个逗号隔开
    private String skuIds;
    private String attrValue;

}
