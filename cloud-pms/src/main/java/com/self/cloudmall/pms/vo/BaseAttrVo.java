package com.self.cloudmall.pms.vo;

import lombok.Data;

@Data
public class BaseAttrVo {

    private Long attrId;
    private String attrValues;
    private int showDesc;



}