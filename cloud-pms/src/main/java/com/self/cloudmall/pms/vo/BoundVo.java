package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BoundVo {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;


}