package com.self.cloudmall.pms.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: CategoryTwoVo
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryTwoVo {
    private String id;
    private String name;
    private String catelog1Id;
    private List<CategoryThreeVo> catalog3List;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CategoryThreeVo{
        private String id;
        private String name;
        private String catelog2Id;
    }

}
