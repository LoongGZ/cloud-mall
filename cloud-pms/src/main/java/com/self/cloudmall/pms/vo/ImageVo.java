package com.self.cloudmall.pms.vo;

import lombok.Data;


@Data
public class ImageVo {

    private String imgUrl;
    private Integer defaultImg;


}