
package com.self.cloudmall.pms.vo;

import lombok.Data;


@Data
public class SkuAttrVo {

    private Long attrId;
    private String attrName;
    private String attrValue;

}