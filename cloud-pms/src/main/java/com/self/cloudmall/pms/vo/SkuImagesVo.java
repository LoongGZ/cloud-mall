package com.self.cloudmall.pms.vo;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: SkuImagesVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class SkuImagesVo {
    /**
     * 图片地址
     */
    private String imgUrl;
    /**
     * 排序
     */
    private Integer imgSort;
    /**
     * 默认图[0 - 不是默认图，1 - 是默认图]
     */
    private Integer defaultImg;
}
