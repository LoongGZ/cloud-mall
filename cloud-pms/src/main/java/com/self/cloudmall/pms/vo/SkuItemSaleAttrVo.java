package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SkuItemSaleAttrVo
 * @Description:
 * @Author: M10003729
 * @Date: 2020/11/6 17:08
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    //属性属于哪几个skuId
    private List<AttrSkuIdVo> attrSkuIds;
}
