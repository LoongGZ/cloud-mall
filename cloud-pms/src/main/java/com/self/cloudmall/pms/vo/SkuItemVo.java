package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SkuItemVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class SkuItemVo {
    //sku信息
    private SkuInfoVo skuInfo;
    //sku图片
    private List<SkuImagesVo> skuImages;
    //是否有库存
    private Boolean hasStock=true;

    //spu的销售属性
    private List<SkuItemSaleAttrVo> saleAttrs;
    //spu介绍
    private String decript;
    //spu的规格参数
    private List<SpuBaseGroupAttrVo> groupAttrVos;
    //秒杀信息
    private SeckillSkuVo seckillSku;
}
