package com.self.cloudmall.pms.vo;
import com.self.cloudmall.common.dto.MemberPriceDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
public class SkuVo {

    private Long skuId;
    private List<SkuAttrVo> attr;
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;
    private List<ImageVo> images;
    private List<String> descar;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPriceDto> memberPrice;


}