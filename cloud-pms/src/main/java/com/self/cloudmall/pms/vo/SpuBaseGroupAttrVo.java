package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SpuBaseGroupAttrVo
 * @Description:
 * @Author: M10003729
 * @Date: 2020/11/6 17:06
 */
@Data
public class SpuBaseGroupAttrVo {
    private String attrGroupName;
    private List<SkuAttrVo> attrs;
}
