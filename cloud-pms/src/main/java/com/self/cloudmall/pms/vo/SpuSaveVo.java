package com.self.cloudmall.pms.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
public class SpuSaveVo {

    private String spuName;
    private String spuDescription;
    private Long catalogId;
    private Long brandId;
    private BigDecimal weight;
    private int publishStatus;
    private List<String> decript;
    private List<String> images;
    private BoundVo bounds;
    private List<BaseAttrVo> baseAttrs;
    private List<SkuVo> skus;



}