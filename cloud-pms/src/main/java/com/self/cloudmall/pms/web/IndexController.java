package com.self.cloudmall.pms.web;

import com.self.cloudmall.pms.entity.CategoryEntity;
import com.self.cloudmall.pms.service.CategoryService;
import com.self.cloudmall.pms.vo.CategoryTwoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @version v1.0
 * @ClassName: index
 * @Description:
 */
@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping({"/","/index.html"})
    public String index(Model model){
        //查询一级分类
        List<CategoryEntity> list = categoryService.queryOneLeveCategorys();
        model.addAttribute("categorys",list);
        //thymeleaf 与springboot整合好的，返回视图后缀默认就是html
        return "index";
    }

    /**
     * @Title: 查询级联分类
     * @Description:
     * @Param []
     */
    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String,List<CategoryTwoVo>> getCategoryJson(){
        return categoryService.getCategoryJson();
    }
}
