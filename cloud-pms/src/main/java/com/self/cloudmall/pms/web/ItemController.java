package com.self.cloudmall.pms.web;

import com.self.cloudmall.pms.service.SkuInfoService;
import com.self.cloudmall.pms.vo.SkuItemVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @version v1.0
 * @ClassName: ItemController
 * @Description:
 */
@Slf4j
@Controller
public class ItemController {

    @Autowired
    private SkuInfoService skuInfoService;


    @GetMapping("/{skuId}.html")
    public String item(@PathVariable("skuId") Long skuId, Model model){
        log.info("商品详情页skuId:{}",skuId);
        SkuItemVo skuItemVo = skuInfoService.getItem(skuId);
        model.addAttribute("item",skuItemVo);
        return "item";
    }
}
