package com.self.cloudmall.pms;

import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
class CloudPmsApplicationTests {

    @Autowired
    private RedissonClient redissonClient;

    @Test
    void lock() throws InterruptedException {
        //阻塞锁，在到期时间后自动会延长时间，延长时间在30/3后出发，然后到30
        //防止死锁，如果一个线程进来了，线程突然中断，当前线程就不会延长
        RLock lock = redissonClient.getLock("my-lock");
        lock.lock();
        System.out.println("---处理业务-");
        Thread.sleep(30000);
        lock.unlock();
    }
    @Test
    void lockTime() throws InterruptedException {
        RLock lock = redissonClient.getLock("my-lock");
        //10秒后自动解锁，不会顺延
        lock.lock(10, TimeUnit.SECONDS);

        System.out.println("---处理业务-");
        Thread.sleep(30000);
        lock.unlock();
    }

    @Test
    void lockRead() throws InterruptedException {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("rw-lock");
        //读锁
        RLock lock = readWriteLock.readLock();
        lock.lock();
        //...
        lock.unlock();
    }
    //写锁是互斥锁，读锁是共享锁，
    //只要存在写锁，都要等待，
    @Test
    void lockWrite() throws InterruptedException {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("rw-lock");
        RLock lock = readWriteLock.writeLock();
        //写锁
        lock.lock();

        //...
        lock.unlock();
    }

    @Test
    void semaphore() throws InterruptedException {
        RSemaphore semaphore = redissonClient.getSemaphore("sema-lock");
        semaphore.trySetPermits(2);//设置信号量容量
        semaphore.acquire();//获取一个许可或者信号，如果获取不到是阻塞

        //尝试获取一个许可，没有获取到返回false
        //分布式限流可以使用这个特性
        boolean b = semaphore.tryAcquire(10, TimeUnit.SECONDS);

        //..
        semaphore.release();//释放一个信号
    }

    @Test
    void countdown() throws InterruptedException {
        RCountDownLatch countDownLatch = redissonClient.getCountDownLatch("latch-locl");
        countDownLatch.trySetCount(2);//设置触发数量
        countDownLatch.await();
        //当countDown把数量减为0触发await
       // countDownLatch.countDown();
    }

}
