package com.self.cloudmall.search.controller;


import com.self.cloudmall.common.dto.ElasticSkuDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.search.service.ProductSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/search/product/")
public class ProductSearchController {

    @Autowired
    private ProductSearchService productSearchService;

    @PostMapping("add")
    public R productAdd(@RequestBody List<ElasticSkuDto> skuDtoList){
        if (CollectionUtils.isEmpty(skuDtoList)){
            return R.error("商品参数为空");
        }
        try {
            return productSearchService.productAdd(skuDtoList);
        } catch (IOException e) {
            log.error("商品索引数据保存异常：{}",e);
            return R.error("商品索引数据保存异常");
        }
    }
}
