package com.self.cloudmall.search.controller;

import com.self.cloudmall.search.service.SearchService;
import com.self.cloudmall.search.vo.SearchParam;
import com.self.cloudmall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @version v1.0
 * @ClassName: SearchController
 * @Description:
 */
@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @GetMapping("/list.html")
    public String listPage(SearchParam searchParam, Model model){
        SearchResult result = searchService.search(searchParam);
        model.addAttribute("result",result);
        return "list";
    }
}
