package com.self.cloudmall.search.service;

import com.self.cloudmall.common.dto.ElasticSkuDto;
import com.self.cloudmall.common.utils.R;

import java.io.IOException;
import java.util.List;

public interface ProductSearchService {
    R productAdd(List<ElasticSkuDto> skuDtoList) throws IOException;
}
