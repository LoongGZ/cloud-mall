package com.self.cloudmall.search.service;

import com.self.cloudmall.search.vo.SearchParam;
import com.self.cloudmall.search.vo.SearchResult;

/**
 * @version v1.0
 * @InterfaceName: SearchService
 * @Description:
 */
public interface SearchService {

    /**
     * @Title: 商品搜索
     * @Description:
     * @Param [searchParam]
     * @Return com.self.cloudmall.search.vo.SearchResult
     */
    SearchResult search(SearchParam searchParam);
}
