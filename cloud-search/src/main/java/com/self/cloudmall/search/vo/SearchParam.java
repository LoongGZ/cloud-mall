package com.self.cloudmall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SearchParam
 * @Description:
 */
@Data
public class SearchParam {
    private String keyword; //全文索引关键字
    private Long catalog3Id;
    /*
     * sort=saleCount_asc/desc
     * sort=hotScore_asc/desc
     * sort=skuPrice_asc/desc
     */
    private String sort; //排序，例如：按销量，价格，评分等

    private Integer hasStock;//是否只显示有货
    /*
     * skuPrice=1_200/_200/200_
     */
    private String skuPrice;//价格区间

    private List<Long> brandId;//品牌搜索，可以选择多个品牌
    /*
     * attrs=1_4核:8核
     */
    private List<String> attrs;//属性筛选

    private Integer pageNum = 1;//页码


}
