package com.self.cloudmall.search.vo;

import com.self.cloudmall.common.dto.ElasticSkuDto;
import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SearchResult
 * @Description:
 */
@Data
public class SearchResult {
    //es检索出的sku信息
    private List<ElasticSkuDto> products;
    /*
     * 分页信息
     */
    private Integer pageNum;
    private Integer totalPage;
    private Long totalRecord;//总记录数
    private List<String> pageNos;
    //当前查询，所涉及的品牌
    private List<BrandVo> brands;
    //当前查询，所涉及的分类
    private List<CatalogVo> catalogs;
    //当前查询，所涉及的属性
    private List<AttrVo> attrs;

    @Data
    public static class BrandVo{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }

    @Data
    public static class CatalogVo{
        private Long catalogId;
        private String catalogName;
    }

    @Data
    public static class AttrVo{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }
}
