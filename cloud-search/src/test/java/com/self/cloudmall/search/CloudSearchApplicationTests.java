package com.self.cloudmall.search;

import com.alibaba.fastjson.JSONObject;
import com.self.cloudmall.search.config.ElasticConfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
class CloudSearchApplicationTests {

    @Autowired
    private RestHighLevelClient elasticClient;

    @Test
    public void addIndex() throws IOException {
        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1");
        User user = new User();
        user.setAge(30);
        user.setGender("男");
        user.setName("王五");
        indexRequest.source(JSONObject.toJSONString(user),XContentType.JSON);
        IndexResponse response = elasticClient.index(indexRequest, ElasticConfig.COMMON_OPTIONS);
        System.out.println(response);
    }

    @Test
    public void searchIndex() throws IOException {
        //创建搜索请求
        SearchRequest searchRequest = new SearchRequest("bank");
        //创建搜索条件builder
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("address","Mill"));
        sourceBuilder.from(0);
        sourceBuilder.size(10);
        sourceBuilder.sort("balance", SortOrder.DESC);
        //aggs聚合
        sourceBuilder.aggregation(AggregationBuilders.terms("ageAgg").field("age").size(10));
        sourceBuilder.aggregation(AggregationBuilders.avg("ageAvg").field("balance"));
        System.out.println("条件："+sourceBuilder);
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = elasticClient.search(searchRequest, ElasticConfig.COMMON_OPTIONS);
        System.out.println("结果："+searchResponse);
        SearchHits hit = searchResponse.getHits();
        System.out.println("总记录数："+hit.getTotalHits().value);
        System.out.println("最大分数："+hit.getMaxScore());
        SearchHit[] hits = hit.getHits();
        for (SearchHit hi : hits){
            String id = hi.getId();
            System.out.println("id："+id);
            String index = hi.getIndex();
            System.out.println("索引库："+index);
            float score = hi.getScore();
            System.out.println("得分："+score);
            String sourceAsString = hi.getSourceAsString();
            System.out.println("显示字段:"+sourceAsString);
            System.out.println("==============================");
        }
        Aggregations aggregations = searchResponse.getAggregations();
        for (Aggregation ag : aggregations.asList()){
            System.out.println(ag);
        }
    }

    @Data
    class User{
        private String name;
        private Integer age;
        private String gender;
    }

}
