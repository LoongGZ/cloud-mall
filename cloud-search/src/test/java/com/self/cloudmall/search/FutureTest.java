package com.self.cloudmall.search;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class FutureTest {

    static ThreadPoolExecutor executor = new ThreadPoolExecutor(10,20,
            30, TimeUnit.SECONDS,new SynchronousQueue<>());

    /**
     * CompletableFuture:线程编排类
     * thenApplyAsync : 上一个方法执行完成，异步回调本方法
     * thenApply：上一个方法执行完成，使用上一个 线程执行本方法
     * exceptionally: 上一个方法报异常，执行此方法
     * handle：上一个执行完成，使用同一个线程执行本方法，并 得到上一个执行结果和捕获异常（thenApply+exceptionally）
     * handleAsync：thenApplyAsync+exceptionally
     *thenRun：不接受上一个方法执行结果，执行本方法
     * @throws Exception
     */
    @Test
    public void runAsnc() throws Exception {
        //无返回值的异步线程
       CompletableFuture.runAsync(() -> {
            System.out.println("当前线程名称：" + Thread.currentThread().getName());
            int x = 10 / 0;
            System.out.println("无返回值的completableFuture:" + x);
        }, executor).handle((res, e) -> {
            System.out.println("上一个执行的返回值：" + res);
            if (e != null) {
                System.out.println("上一个执行异常 信息：" + e.getMessage());
                return 12;
            }
            return res;
        }).thenRun(()->{
           System.out.println("thenRun-上一个任务执行完成，执行本方法");
       });
        System.out.println("==============================");

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程名称：" + Thread.currentThread().getName());
            int x = 10 / 2;
            System.out.println("有返回值的completableFuture:" + x);
            return x;
        }, executor).thenApplyAsync(res->{
            System.out.println("当前线程名称：" + Thread.currentThread().getName());
            System.out.println("上一个执行的返回值：" + res);
            return res;
        }).exceptionally(e->{
            System.out.println("上一个执行异常 信息：" + e.getMessage());
            return 11;
        });
        Integer integer = future.get();
        System.out.println("返回值：" + integer);
    }

    /**
     * runAfterBothAsync:两个完成，没有获取前两个任务返回结果，没有返回值
     * thenAcceptBothAsync:两个完成，获取前两个任务返回结果，没有返回值
     * thenCombineAsync:两个任务都完成，获取前两个任务返回结果，本方法返回 结果
     * @throws Exception
     */
    @Test
    public void runCombine() throws Exception {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务1-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务1执行完成");
            return "task1-hello";
        }, executor);
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务2-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务2执行完成");
            return "task2-world";
        }, executor);
        //两个完成，没有获取前两个任务返回结果，没有返回值
        future1.runAfterBothAsync(future2,()->{
            System.out.println("runAfterBothAsync-f1完成，f2完成，->本方法执行完成：" + Thread.currentThread().getName());
        },executor);
        System.out.println("==============================");
        //两个任务都执行完成，获取前两个任务返回结果，本方法不返回结果；
        future2.thenAcceptBothAsync(future1,(f1,f2) ->{
            System.out.println("thenAcceptBothAsync-f1完成结果："+f2+"，f2完成结果:"+f1+"，->本方法执行完成：" + Thread.currentThread().getName());
        },executor);
        System.out.println("==============================");
        //两个任务都完成，获取前两个任务返回结果，本方法返回 结果
        CompletableFuture<String> future3 = future1.thenCombineAsync(future2, (f1, f2) -> {
            System.out.println("thenCombineAsync-f1完成结果：" + f1 + "，f2完成结果:" + f2 + "，->本方法执行完成：" + Thread.currentThread().getName());
            return f1 + "-" + f2 + "-" + "task3-java";
        }, executor);
        String s = future3.get();
        System.out.println("task3返回值：" + s);
    }

    /**
     *runAfterEitherAsync:两个任意一个线程完成，执行本方法，不获取前两个返回值结果，本方法没有返回结果
     * acceptEitherAsync：....获取前两个返回值结果，本方法没有返回结果,
     * applyToEitherAsync:....获取前两个返回值结果，本方法有返回结果,
     * @throws Exception
     */
    @Test
    public void runEither() throws Exception {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务1-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务1执行完成");
            return "task1-hello";
        }, executor);
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务2-当前线程名称：" + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("任务2执行完成");
            return "task2-world";
        }, executor);
       // System.out.println("==============================");
       /* future1.runAfterEitherAsync(future2,()->{
            System.out.println("任务3-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务2,任务1任意一个执行完成后执行,没有返回结果");
        },executor);*/

     /*   System.out.println("==============================");
        future1.acceptEitherAsync(future2,res->{
            System.out.println("任务3-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务2,任务1任意一个执行完成后结果："+res);
        },executor);*/
        System.out.println("==============================");
        future1.applyToEitherAsync(future2,res->{
            System.out.println("任务3-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务2,任务1任意一个执行完成后结果："+res);
            return "task3-java";
        },executor);
       /* try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }

    /**
     * allOf；所以任务完成后，每个任务都可以获取到结果
     * anyOf：任意一个任务完成，可以获取到完成的任务结果
     * @throws Exception
     */
    @Test
    public void runSync() throws Exception {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务1-当前线程名称：" + Thread.currentThread().getName());
            System.out.println("任务1执行完成");
            return "task1-hello";
        }, executor);
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务2-当前线程名称：" + Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("任务2执行完成");
            return "task2-world";
        }, executor);
        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("任务3-当前线程名称：" + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("任务3执行完成");
            return "task2-world";
        }, executor);
       /* CompletableFuture<Void> allOf = CompletableFuture.allOf(future1, future2, future3);
        allOf.get();*/
        CompletableFuture<Object> anyOf = CompletableFuture.anyOf(future1, future2, future3);
        Object o = anyOf.get();
        System.out.println("任意完成的结果："+o);
        System.out.println("f1=>"+future1.get()+",f2=>"+future2.get()+",f3=>"+ future3.get());

    }
}
