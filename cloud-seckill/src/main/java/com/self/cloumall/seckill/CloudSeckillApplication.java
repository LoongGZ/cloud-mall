package com.self.cloumall.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @Title: sentinel用法：
 *      1.通用： 导入sentinel，自动为每个请求加上限流；也可以为所有url指定统一的限流返回结果格式
 *      2.嵌入代码： try(Entry entry = SphU.entry("SeckillSkus")){}catch (BlockException e){}
 *      3.注解形式： @SentinelResource(value = "getSeckillSkuBySkuId",
 *             blockHandler = "seckillSkuBySkuIdBlockHandler",
 *             blockHandlerClass = SeckillSkuFallback.class,
 *             fallback = "seckillSkuBySkuIdFallback",
 *             fallbackClass = SeckillSkuFallback.class)
 * @Author M10003729
 */
@EnableRedisHttpSession
@EnableFeignClients(basePackages = "com.self.cloumall.seckill.client")
@EnableDiscoveryClient
@SpringBootApplication
public class CloudSeckillApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudSeckillApplication.class, args);
    }

}
