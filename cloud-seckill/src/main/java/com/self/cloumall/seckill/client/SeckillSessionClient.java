package com.self.cloumall.seckill.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @version v1.0
 * @ClassName: SeckillSessionClient
 * @Description:
 * @Author: M10003729
 */
@FeignClient("cloud-sms")
public interface SeckillSessionClient {

    @GetMapping("/sms/seckillsession/latest3Days")
    R getLatest3Days();
}
