package com.self.cloumall.seckill.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
public class RabbitMqConfig {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * 使用Json序列化
     *
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @PostConstruct
    public void init(){
        //消息抵达交换机回调方法
        /**
         * correlationData:消息关联数据，可以设置唯一ID
         * isAck：是否确认成功，
         * error：错误信息
         */
        rabbitTemplate.setConfirmCallback((correlationData, isAck, error) -> {
            if (!isAck) {
                log.error("秒杀订单-correlationData:{},消息没有确认，错误信息：{}", correlationData, error);
            }
            log.info("秒杀订单-correlationData:{},消息确认完成", correlationData);
        });
        //消息抵达队列回调方法
        /**
         * message：消息
         * replayCode：回复状态码
         * replayText：回复文本
         * exchange：消息来自于的交换机
         * routingKey：消息用的路由键
         */
        rabbitTemplate.setReturnCallback((Message message, int replayCode, String replayText, String exchange, String routingKey) -> {
            log.info("秒杀订单-消息：{}，回复状态码：{}，回复文本：{}，消息来自于的交换机：{}，消息用的路由键：{}", message,
                    replayCode, replayText, exchange, routingKey);
        });
    }
}