package com.self.cloumall.seckill.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @version v1.0
 * @ClassName: ScheduleConfig
 * @Description:
 * @Author: M10003729
 */
@Configuration
@EnableAsync
@EnableScheduling
public class ScheduleConfig {


}
