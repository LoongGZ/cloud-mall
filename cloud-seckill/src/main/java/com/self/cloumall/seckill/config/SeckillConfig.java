package com.self.cloumall.seckill.config;

import com.self.cloumall.seckill.interceptor.ValidUserLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SeckillConfig implements WebMvcConfigurer {

    @Autowired
    private ValidUserLoginInterceptor validUserLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(validUserLoginInterceptor).addPathPatterns("/**");
    }
}
