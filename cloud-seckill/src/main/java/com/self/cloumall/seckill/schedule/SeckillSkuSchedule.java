package com.self.cloumall.seckill.schedule;

import com.self.cloudmall.common.constant.SeckillConstant;
import com.self.cloumall.seckill.service.SeckillSkuService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @ClassName: SeckillSch
 * @Description:
 * @Author: M10003729
 */
@Component
@Slf4j
public class SeckillSkuSchedule {

    @Autowired
    private SeckillSkuService seckillSkuService;
    @Autowired
    private RedissonClient redissonClient;

    /**
     * @Title: 每天三点上架最近三天的商品
     * @Description:
     * @Param []
     * @Return void
     * @Author M10003729
     * @Throws
     */
    @Async
    @Scheduled(cron = "0 0 3 * * ?")
    public void uploadSeckillSkuLatest3Days(){
        //重复的无须处理
        RLock lock = redissonClient.getLock(SeckillConstant.UPLOAD_SKU_LOCK);
        try {
            lock.lock();
            seckillSkuService.uploadSeckillSkuLatest3Days();
        }finally {
            lock.unlock();
        }
    }
}
