package com.self.cloumall.seckill.service;

import com.self.cloudmall.common.dto.SeckillSkuDto;
import com.self.cloudmall.common.utils.R;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: SeckillSkuService
 * @Description:
 * @Author: M10003729
 */
public interface SeckillSkuService {

    void uploadSeckillSkuLatest3Days();

    List<SeckillSkuDto> getCurSeckillSkus();

    SeckillSkuDto getSeckillSkuBySkuId(Long skuId);

    R seckillSku(String killId, Integer num, String key);
}
