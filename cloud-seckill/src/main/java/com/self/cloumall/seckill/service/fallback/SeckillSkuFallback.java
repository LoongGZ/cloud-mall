package com.self.cloumall.seckill.service.fallback;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.self.cloudmall.common.dto.SeckillSkuDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @ClassName: SeckillSkuFallback
 * @Description: 必须都是静态方法
 * @Author: M10003729
 */
@Component
@Slf4j
public class SeckillSkuFallback {
    public static SeckillSkuDto seckillSkuBySkuIdBlockHandler(Long skuId, BlockException e){
        log.error("秒杀商品id：{},获取商品信息限流，限流信息：{}",skuId,e);
        return null;
    }
    public static SeckillSkuDto seckillSkuBySkuIdFallback(Long skuId,Exception e){
        log.error("秒杀商品id：{},获取商品信息降级,降级原因：{}",skuId,e);
        return null;
    }
}
