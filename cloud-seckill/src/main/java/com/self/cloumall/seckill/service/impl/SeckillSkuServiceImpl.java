package com.self.cloumall.seckill.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.self.cloudmall.common.constant.SeckillConstant;
import com.self.cloudmall.common.dto.LoginUserDto;
import com.self.cloudmall.common.dto.SeckillOrderDto;
import com.self.cloudmall.common.dto.SeckillSkuDto;
import com.self.cloudmall.common.dto.SkuInfoDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.common.utils.ThreadLocalUtil;
import com.self.cloudmall.common.utils.UUIDUtil;
import com.self.cloumall.seckill.client.SeckillSessionClient;
import com.self.cloumall.seckill.client.SkuClient;
import com.self.cloumall.seckill.service.SeckillSkuService;
import com.self.cloumall.seckill.service.fallback.SeckillSkuFallback;
import com.self.cloumall.seckill.vo.SeckillSessionVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ClassName: SeckillSkuServiceImpl
 * @Description:
 * @Author: M10003729
 */
@Service
@Slf4j
public class SeckillSkuServiceImpl implements SeckillSkuService {
    @Autowired
    private SeckillSessionClient seckillSessionClient;
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private SkuClient skuClient;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void uploadSeckillSkuLatest3Days() {
        R r = seckillSessionClient.getLatest3Days();
        if (r.isSuccess()){
            List<SeckillSessionVo> seckillSessionVos = JSONObject.parseObject(JSONObject.toJSONString(r.getData()),
                    new TypeReference<List<SeckillSessionVo>>() {
                    });
            //缓存活动场次信息
            cachePromotionSession(seckillSessionVos);
            //缓存场次关联的商品信息
            cacheSessionSku(seckillSessionVos);
        }else{
            log.error("上架最近三天的商品数据异常：{}", JSONObject.toJSONString(r));
        }
    }

    @Override
    public List<SeckillSkuDto> getCurSeckillSkus() {
        //代码形式
        try(Entry entry = SphU.entry("SeckillSkus")){
            long time = new Date().getTime();
            Set<String> keys = redisTemplate.keys(SeckillConstant.PROMOTIONSESSION_PREFIX + "*");
            if (!CollectionUtils.isEmpty(keys)){
                for (String key : keys){
                    String[] split = key.replace(SeckillConstant.PROMOTIONSESSION_PREFIX, "").split("_");
                    long start = Long.parseLong(split[0]);
                    long end = Long.parseLong(split[1]);
                    if (time >= start && time <= end){
                        List<Object> range = redisTemplate.opsForList().range(key, 0, -1);
                        List<Object> objects = redisTemplate.opsForHash().multiGet(SeckillConstant.SESSION_SKU_PREFIX, range);
                        List<SeckillSkuDto> skuDtos = JSONObject.parseObject(JSONObject.toJSONString(objects),
                                new TypeReference<List<SeckillSkuDto>>() {
                        });
                        return skuDtos;
                    }
                }
            }
        }catch (BlockException e){
            log.error("发生限流：{}",e);
        }
        return null;
    }

    @SentinelResource(value = "getSeckillSkuBySkuId",
            fallback = "seckillSkuBySkuIdFallback",
             fallbackClass = SeckillSkuFallback.class)
    @Override
    public SeckillSkuDto getSeckillSkuBySkuId(Long skuId) {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BoundHashOperations<String, String, Object> hashOps = redisTemplate.boundHashOps(SeckillConstant.SESSION_SKU_PREFIX);
        Set<String> keys = hashOps.keys();
        if (!CollectionUtils.isEmpty(keys)){
            String reg = "\\d_"+skuId;
            for (String key : keys){
                if (Pattern.matches(reg,key)){
                    Object obj = hashOps.get(key);
                    SeckillSkuDto skuDto = JSONObject.parseObject(JSONObject.toJSONString(obj), SeckillSkuDto.class);
                    long time = new Date().getTime();
                    if (time > skuDto.getEndTime() || time < skuDto.getStartTime()){
                        skuDto.setRandomCode(null);
                    }
                    return skuDto;
                }
            }

        }
        return null;
    }

    @Override
    public R seckillSku(String killId, Integer num, String key) {
        //查询sku
        BoundHashOperations<String, String, Object> hashOps = redisTemplate.boundHashOps(SeckillConstant.SESSION_SKU_PREFIX);
        Object obj = hashOps.get(killId);
        if (obj == null){
            return R.error("秒杀商品不存在或者活动结束");
        }
        SeckillSkuDto skuDto = JSONObject.parseObject(JSONObject.toJSONString(obj), SeckillSkuDto.class);
        long time = new Date().getTime();
        Long startTime = skuDto.getStartTime();
        Long endTime = skuDto.getEndTime();
        //校验是否在秒杀时间区间
        if(time > endTime || time < startTime){
            return R.error("秒杀活动结束");
        }
        //校验数量数超过限制
        if (num > skuDto.getSeckillLimit()){
            return R.error("秒杀商品数量超过限制");
        }
        //校验killId和key是否和缓存的一致
        String randomCode = skuDto.getRandomCode();
        String temp = skuDto.getPromotionSessionId()+"_"+skuDto.getSkuId();
        if (!temp.equals(killId) || !key.equals(randomCode)){
            return R.error("秒杀随机码不正确");
        }
        //已经购买的，不能重复买 ,key 格式 usrId:PromotionSessionId_SkuId ; value:数量
        long expire = endTime - time;
        LoginUserDto user = (LoginUserDto)ThreadLocalUtil.get();
        Boolean absent = redisTemplate.opsForValue().setIfAbsent(SeckillConstant.SECKILL_USER_PREFIX + user.getId() + ":" + temp,
                num, expire, TimeUnit.MILLISECONDS);
        if (!absent){
            return R.error("不能重复购买该商品");
        }
        //扣减信号量
        RSemaphore semaphore = redissonClient.getSemaphore(SeckillConstant.SEMAPHORE_SKU_STOCK + randomCode);
        try {
            boolean b = semaphore.tryAcquire(num, 100, TimeUnit.MILLISECONDS);
            if (!b){
                return R.error("秒杀排队中，请稍后再试");
            }
        } catch (InterruptedException e) {
            log.error("信号量扣减失败：{}",e);
            return R.error("秒杀排队中，请稍后再试");
        }
        //发送订单消息
        String id = IdWorker.getTimeId();
        SeckillOrderDto seckillOrder = new SeckillOrderDto();
        seckillOrder.setMemberId(user.getId());
        seckillOrder.setNum(num);
        seckillOrder.setOrderSn(id);
        seckillOrder.setPromotionSessionId(skuDto.getPromotionSessionId());
        seckillOrder.setSeckillPrice(skuDto.getSeckillPrice());
        seckillOrder.setSkuId(skuDto.getSkuId());
        seckillOrder.setMemberUsername(user.getUsername());
        rabbitTemplate.convertAndSend("order-seckill-event-exchange","order.seckill.order",seckillOrder);

        return R.ok().setData(id);
    }

    /**
     * @Title: cacheSessionSku
     * @Description: hash结构  key格式：前缀+promotionSessionId_skuId
     *                         value: 秒杀sku信息+商品信息+随机码
     * @Param [seckillSessionVos]
     * @Return void
     * @Author M10003729
     * @Throws
     */
    private void cacheSessionSku(List<SeckillSessionVo> seckillSessionVos) {
        seckillSessionVos.forEach(x -> {
            BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(SeckillConstant.SESSION_SKU_PREFIX);
            x.getSkuRelationList().forEach(y -> {
                //幂等性处理
                if (!hashOps.hasKey(y.getPromotionSessionId() + "_" + y.getSkuId())) {
                    SeckillSkuDto seckillSkuDto = new SeckillSkuDto();
                    //1.秒杀sku信息
                    BeanUtils.copyProperties(y, seckillSkuDto);
                    seckillSkuDto.setStartTime(x.getStartTime().getTime());
                    seckillSkuDto.setEndTime(x.getEndTime().getTime());
                    //2.商品信息
                    R skuInfo = skuClient.getSkuInfo(y.getSkuId());
                    if (skuInfo.isSuccess()) {
                        seckillSkuDto.setSkuInfoDto(JSONObject.parseObject(JSONObject.toJSONString(skuInfo.getData()), SkuInfoDto.class));
                    }
                    //3.随机码
                    String uuid = UUIDUtil.generateUUID();
                    seckillSkuDto.setRandomCode(uuid);
                    //4.设置信号量
                    RSemaphore semaphore = redissonClient.getSemaphore(SeckillConstant.SEMAPHORE_SKU_STOCK + uuid);
                    semaphore.trySetPermits(y.getSeckillCount());
                    hashOps.put(y.getPromotionSessionId() + "_" + y.getSkuId(), seckillSkuDto);
                }
            });
        });
    }

    /**
     * @Title: cachePromotionSession
     * @Description: list结构 key格式：前缀+活动开始时间_活动结束时间
     *               value： promotionSessionId_skuId
     * @Param [seckillSessionVos]
     * @Return void
     * @Author M10003729
     * @Throws
     */
    private void cachePromotionSession(List<SeckillSessionVo> seckillSessionVos) {
        seckillSessionVos.forEach(x -> {

            if (x.getStartTime() != null && x.getEndTime() != null){
                String key = SeckillConstant.PROMOTIONSESSION_PREFIX+x.getStartTime().getTime()+"_"+x.getEndTime().getTime();
                //幂等性处理
                if (!redisTemplate.hasKey(key)){
                    List<String> collect = x.getSkuRelationList().stream()
                            .map(y -> y.getPromotionSessionId()+"_"+y.getSkuId()).collect(Collectors.toList());
                    redisTemplate.opsForList().leftPushAll(key,collect);
                }
            }
        });

    }
}
