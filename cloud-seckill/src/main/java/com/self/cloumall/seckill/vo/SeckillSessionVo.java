package com.self.cloumall.seckill.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @version v1.0
 * @ClassName: SeckillSessionVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class SeckillSessionVo {
    private Long id;
    /**
     * 场次名称
     */
    private String name;
    /**
     * 每日开始时间
     */
    private Date startTime;
    /**
     * 每日结束时间
     */
    private Date endTime;
    /**
     * 启用状态
     */
    private Integer status;

    private List<SeckillSkuVo> skuRelationList;
}
