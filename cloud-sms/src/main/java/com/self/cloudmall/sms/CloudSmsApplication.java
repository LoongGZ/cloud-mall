package com.self.cloudmall.sms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.self.cloudmall.sms.client")
@EnableDiscoveryClient
@MapperScan("com.self.cloudmall.sms.mapper")
@SpringBootApplication
public class CloudSmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudSmsApplication.class, args);
    }

}
