package com.self.cloudmall.sms.mapper;

import com.self.cloudmall.sms.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
@Mapper
public interface MemberPriceMapper extends BaseMapper<MemberPriceEntity> {
	
}
