package com.self.cloudmall.sms.mapper;

import com.self.cloudmall.sms.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
@Mapper
public interface SeckillPromotionMapper extends BaseMapper<SeckillPromotionEntity> {
	
}
