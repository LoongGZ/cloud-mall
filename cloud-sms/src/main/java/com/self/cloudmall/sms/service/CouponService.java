package com.self.cloudmall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

