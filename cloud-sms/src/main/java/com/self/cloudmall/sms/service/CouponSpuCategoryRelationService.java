package com.self.cloudmall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.entity.CouponSpuCategoryRelationEntity;

import java.util.Map;

/**
 * 优惠券分类关联
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
public interface CouponSpuCategoryRelationService extends IService<CouponSpuCategoryRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

