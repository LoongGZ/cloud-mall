package com.self.cloudmall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.entity.SeckillSessionEntity;

import java.util.List;
import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SeckillSessionEntity> getLatest3Days();
}

