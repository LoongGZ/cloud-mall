package com.self.cloudmall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.dto.FullReductionDto;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.entity.SkuFullReductionEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品满减信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:21:23
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean batchAdd(List<FullReductionDto> dtos);
}

