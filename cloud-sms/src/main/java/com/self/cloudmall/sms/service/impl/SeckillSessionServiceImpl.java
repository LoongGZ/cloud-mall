package com.self.cloudmall.sms.service.impl;

import com.self.cloudmall.common.utils.DateUtil;
import com.self.cloudmall.sms.entity.SeckillSkuRelationEntity;
import com.self.cloudmall.sms.service.SeckillSkuRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.utils.Query;

import com.self.cloudmall.sms.mapper.SeckillSessionMapper;
import com.self.cloudmall.sms.entity.SeckillSessionEntity;
import com.self.cloudmall.sms.service.SeckillSessionService;
import org.springframework.util.CollectionUtils;


@Service("seckillSessionService")
public class SeckillSessionServiceImpl extends ServiceImpl<SeckillSessionMapper, SeckillSessionEntity> implements SeckillSessionService {

    @Autowired
    private SeckillSkuRelationService seckillSkuRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillSessionEntity> page = this.page(
                new Query<SeckillSessionEntity>().getPage(params),
                new QueryWrapper<SeckillSessionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SeckillSessionEntity> getLatest3Days() {
        List<SeckillSessionEntity> list = this.lambdaQuery().between(SeckillSessionEntity::getStartTime, DateUtil.getCurStartTime(), DateUtil.getEndTime(2))
                .list();
        if (!CollectionUtils.isEmpty(list)){
            List<SeckillSessionEntity> collect = list.stream().map(x -> {
                List<SeckillSkuRelationEntity> relationEntities = seckillSkuRelationService.lambdaQuery()
                        .eq(SeckillSkuRelationEntity::getPromotionSessionId, x.getId())
                        .list();
                x.setSkuRelationList(relationEntities);
                return x;
            }).collect(Collectors.toList());
            return collect;
        }
        return list;
    }

}