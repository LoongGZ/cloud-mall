package com.self.cloudmall.sms.service.impl;

import com.self.cloudmall.common.dto.FullReductionDto;
import com.self.cloudmall.common.dto.MemberPriceDto;
import com.self.cloudmall.sms.entity.MemberPriceEntity;
import com.self.cloudmall.sms.entity.SkuLadderEntity;
import com.self.cloudmall.sms.service.MemberPriceService;
import com.self.cloudmall.sms.service.SkuLadderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.sms.utils.PageUtils;
import com.self.cloudmall.sms.utils.Query;

import com.self.cloudmall.sms.mapper.SkuFullReductionMapper;
import com.self.cloudmall.sms.entity.SkuFullReductionEntity;
import com.self.cloudmall.sms.service.SkuFullReductionService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionMapper, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    private SkuLadderService skuLadderService;
    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean batchAdd(List<FullReductionDto> dtos) {
        if (CollectionUtils.isEmpty(dtos)){
            return false;
        }
        dtos.stream().forEach(x -> {
            //满减
            SkuFullReductionEntity fullReductionEntity = new SkuFullReductionEntity();
            if(x.getFullCount() > 0){
                BeanUtils.copyProperties(x,fullReductionEntity);
                fullReductionEntity.setAddOther(x.getCountStatus());
                this.baseMapper.insert(fullReductionEntity);
            }
            //商品阶梯价格
            if(x.getFullCount() > 0 && x.getDiscount().compareTo(BigDecimal.ZERO) > 0){
                SkuLadderEntity ladderEntity = new SkuLadderEntity();
                BeanUtils.copyProperties(x,ladderEntity);
                ladderEntity.setAddOther(x.getCountStatus());
                skuLadderService.save(ladderEntity);
            }
            //会员价格
            List<MemberPriceDto> priceDtoList = x.getMemberPrice();
            List<MemberPriceEntity> priceEntities = assembleMemberPriceDto(priceDtoList,x);
            if (!CollectionUtils.isEmpty(priceEntities)){
                memberPriceService.saveBatch(priceEntities);
            }
        });
        return true;
    }

    private List<MemberPriceEntity> assembleMemberPriceDto(List<MemberPriceDto> priceDtoList,FullReductionDto reductionDto) {
        if (!CollectionUtils.isEmpty(priceDtoList)){
            return priceDtoList.stream().filter(item-> item.getPrice().compareTo(BigDecimal.ZERO) > 0)
                    .map(x ->{
                MemberPriceEntity priceEntity = new MemberPriceEntity();
                priceEntity.setMemberLevelName(x.getName());
                priceEntity.setMemberPrice(x.getPrice());
                priceEntity.setMemberLevelId(x.getId());
                priceEntity.setAddOther(1);
                priceEntity.setSkuId(reductionDto.getSkuId());
                return priceEntity;
            }).collect(Collectors.toList());
        }
        return null;
    }

}