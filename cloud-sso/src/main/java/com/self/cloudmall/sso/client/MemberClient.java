package com.self.cloudmall.sso.client;

import com.self.cloudmall.common.dto.SocialLoginDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.sso.vo.LoginUserVo;
import com.self.cloudmall.sso.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @version v1.0
 * @ClassName: MemberClient
 * @Description:
 * @Author: M10003729
 */
@FeignClient("cloud-ums")
public interface MemberClient {
    @PostMapping("/ums/member/regist")
    R regist(@RequestBody UserRegistVo memberRegVo);

    @PostMapping("/ums/member/login")
    R login(@RequestBody LoginUserVo loginUserVo);

    @PostMapping("/ums/member/socialLogin")
    R socialLogin(@RequestBody SocialLoginDto socialLoginDto);

}
