package com.self.cloudmall.sso.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.cloud.aliyun.sms")
@Data
public class SmsConfig {
    private String accessKey;
    private String accessSecret;
    private String signName;
    private String templateCode;
}
