package com.self.cloudmall.sso.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @version v1.0
 * @ClassName: WebMvcConfig
 * @Description:
 * @Author: M10003729
 */
@EnableWebMvc
@Configuration
public class WebMvcConfig implements WebMvcConfigurer{

    /**
     * @Title: 添加视图，更轻量级跳转
     * @Description:
     * @Param [registry]
     * @Return void
     * @Author M10003729
     * @Throws
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //registry.addViewController("login.html").setViewName("login");
        registry.addViewController("register.html").setViewName("register");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/templates/");
    }
}
