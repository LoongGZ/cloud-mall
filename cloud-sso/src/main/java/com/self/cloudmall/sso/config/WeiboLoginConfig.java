package com.self.cloudmall.sso.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @ClassName: SocialLoginConfig
 * @Description:
 * @Author: M10003729
 */
@Component
@Data
@ConfigurationProperties(prefix = "login.social.weibo")
public class WeiboLoginConfig {
    private String appKey;
    private String appSecret;
    private String accessUrl;
    private String userInfoUrl;
}
