package com.self.cloudmall.sso.service;

import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.sso.vo.LoginUserVo;

public interface LoginService {
    R sendCode(String phone);

    R login(LoginUserVo loginUserVo);

    R socialLogin(String code);
}
