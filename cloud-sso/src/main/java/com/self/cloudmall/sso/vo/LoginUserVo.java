package com.self.cloudmall.sso.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @version v1.0
 * @ClassName: LoginUserVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class LoginUserVo {
    @NotBlank(message = "用户名不为空")
    private String loginAccount;
    @NotBlank(message = "密码不能为空")
    @Length(min = 6,max = 18,message = "用户名必须在6-18个字符之间")
    private String password;

    private String originUrl;
}
