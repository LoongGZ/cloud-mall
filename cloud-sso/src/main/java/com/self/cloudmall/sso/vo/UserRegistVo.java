package com.self.cloudmall.sso.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

@Data
public class UserRegistVo {
    @NotBlank(message = "用户名不为空")
    @Length(min = 4,max = 20,message = "用户名必须在4-20个字符之间")
    private String username;
    @NotBlank(message = "密码不能为空")
    @Length(min = 6,max = 18,message = "用户名必须在6-18个字符之间")
    private String password;
    @NotBlank(message = "手机号不为空")
    @Pattern(regexp="^[1]([3-9])[0-9]{9}$",message = "手机号码格式不对")
    private String phone;
    @NotBlank(message = "验证码不为空")
    private String code;
}
