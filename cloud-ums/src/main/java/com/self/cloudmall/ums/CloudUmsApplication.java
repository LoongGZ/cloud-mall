package com.self.cloudmall.ums;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession
@EnableFeignClients(basePackages = "com.self.cloudmall.ums.client")
@EnableDiscoveryClient
@MapperScan("com.self.cloudmall.ums.mapper")
@SpringBootApplication
public class CloudUmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudUmsApplication.class, args);
    }

}
