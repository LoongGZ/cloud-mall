package com.self.cloudmall.ums.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient("cloud-oms")
public interface OrderClient {

    @PostMapping("/oms/order/orderList")
    R orderList(@RequestBody Map<String, Object> params);
}
