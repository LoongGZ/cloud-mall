package com.self.cloudmall.ums.config;

import com.self.cloudmall.ums.interceptor.ValidUserLoginInterceptor;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class MemberMvcConfig implements WebMvcConfigurer {

    @Autowired
    private ValidUserLoginInterceptor validUserLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(validUserLoginInterceptor).addPathPatterns("/**");
    }

    @Bean
    public RequestInterceptor requestInterceptor(){
        return template ->{
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null){
                HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
                //把老的请求头cookie赋值给新的requestTemplate
                template.header("Cookie",request.getHeader("Cookie"));
            }
        };
    }
}
