package com.self.cloudmall.ums.controller;

import java.util.Arrays;
import java.util.Map;

import com.self.cloudmall.common.dto.SocialLoginDto;
import com.self.cloudmall.ums.vo.LoginMemberVo;
import com.self.cloudmall.ums.vo.MemberRegVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.self.cloudmall.ums.entity.MemberEntity;
import com.self.cloudmall.ums.service.MemberService;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.common.utils.R;



/**
 * 会员
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
@RestController
@RequestMapping("ums/member")
public class MemberController {
    @Autowired
    private MemberService memberService;


    /**
     * @Title: 注册会员
     */
    @PostMapping("/regist")
    public R regist(@RequestBody MemberRegVo memberRegVo){
        return memberService.regist(memberRegVo);
    }

    /**
     * @Title: 登录
     */
    @PostMapping("/login")
    public R login(@RequestBody LoginMemberVo loginMemberVo){
        return memberService.login(loginMemberVo);
    }

    @PostMapping("/socialLogin")
    public R socialLogin(@RequestBody SocialLoginDto socialLoginDto){
        return memberService.socialLogin(socialLoginDto);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("ums:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("ums:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ums:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ums:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ums:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
