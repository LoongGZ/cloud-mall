package com.self.cloudmall.ums.mapper;

import com.self.cloudmall.ums.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
@Mapper
public interface MemberCollectSpuMapper extends BaseMapper<MemberCollectSpuEntity> {
	
}
