package com.self.cloudmall.ums.mapper;

import com.self.cloudmall.ums.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
@Mapper
public interface MemberCollectSubjectMapper extends BaseMapper<MemberCollectSubjectEntity> {
	
}
