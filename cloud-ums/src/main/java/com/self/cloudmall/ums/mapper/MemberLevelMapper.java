package com.self.cloudmall.ums.mapper;

import com.self.cloudmall.ums.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
@Mapper
public interface MemberLevelMapper extends BaseMapper<MemberLevelEntity> {
	
}
