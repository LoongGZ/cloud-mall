package com.self.cloudmall.ums.mapper;

import com.self.cloudmall.ums.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
@Mapper
public interface MemberStatisticsInfoMapper extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
