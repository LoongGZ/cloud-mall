package com.self.cloudmall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.entity.GrowthChangeHistoryEntity;

import java.util.Map;

/**
 * 成长值变化历史记录
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
public interface GrowthChangeHistoryService extends IService<GrowthChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

