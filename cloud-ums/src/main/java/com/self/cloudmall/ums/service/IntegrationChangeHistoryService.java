package com.self.cloudmall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

