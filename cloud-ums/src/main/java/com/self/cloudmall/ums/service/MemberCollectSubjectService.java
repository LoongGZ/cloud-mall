package com.self.cloudmall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.entity.MemberCollectSubjectEntity;

import java.util.Map;

/**
 * 会员收藏的专题活动
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
public interface MemberCollectSubjectService extends IService<MemberCollectSubjectEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

