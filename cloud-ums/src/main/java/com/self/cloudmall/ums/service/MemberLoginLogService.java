package com.self.cloudmall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

