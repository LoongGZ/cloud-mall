package com.self.cloudmall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.dto.SocialLoginDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.entity.MemberEntity;
import com.self.cloudmall.ums.vo.LoginMemberVo;
import com.self.cloudmall.ums.vo.MemberRegVo;

import java.util.Map;

/**
 * 会员
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 19:16:28
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R regist(MemberRegVo memberRegVo);

    R login(LoginMemberVo loginMemberVo);

    R socialLogin(SocialLoginDto socialLoginDto);

    R orderList(Map<String, Object> params);
}

