package com.self.cloudmall.ums.service.impl;

import com.self.cloudmall.common.constant.PmsConstant;
import com.self.cloudmall.common.utils.R;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.ums.utils.PageUtils;
import com.self.cloudmall.ums.utils.Query;

import com.self.cloudmall.ums.mapper.MemberReceiveAddressMapper;
import com.self.cloudmall.ums.entity.MemberReceiveAddressEntity;
import com.self.cloudmall.ums.service.MemberReceiveAddressService;


@Service("memberReceiveAddressService")
public class MemberReceiveAddressServiceImpl extends ServiceImpl<MemberReceiveAddressMapper, MemberReceiveAddressEntity> implements MemberReceiveAddressService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberReceiveAddressEntity> page = this.page(
                new Query<MemberReceiveAddressEntity>().getPage(params),
                new QueryWrapper<MemberReceiveAddressEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public R getAddressList(Long memberId) {
        List<MemberReceiveAddressEntity> list = this.lambdaQuery().eq(MemberReceiveAddressEntity::getMemberId, memberId)
                .list();
        return R.ok().setData(list);
    }

}