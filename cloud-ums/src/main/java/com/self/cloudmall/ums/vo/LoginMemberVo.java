package com.self.cloudmall.ums.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @version v1.0
 * @ClassName: LoginUserVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class LoginMemberVo {
    private String loginAccount;
    private String password;
}
