package com.self.cloudmall.ums.vo;

import lombok.Data;

/**
 * @version v1.0
 * @ClassName: MemberRegVo
 * @Description:
 * @Author: M10003729
 */
@Data
public class MemberRegVo {
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号码
     */
    private String phone;
}
