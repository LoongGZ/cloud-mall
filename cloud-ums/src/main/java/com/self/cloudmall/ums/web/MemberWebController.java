package com.self.cloudmall.ums.web;


import com.google.common.collect.Maps;
import com.self.cloudmall.common.utils.Constant;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.ums.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class MemberWebController {

    @Autowired
    private MemberService memberService;

    @GetMapping("/home")
    public String home(@RequestParam(name = "pageNum",defaultValue = "1") String pageNum,
                       @RequestParam(name = "pageSize",defaultValue = "10") String pageSize,Model model){
        Map<String,Object> params = Maps.newHashMap();
        params.put(Constant.PAGE,pageNum);
        params.put(Constant.LIMIT,pageSize);
        R r = memberService.orderList(params);
        model.addAttribute("orders",r.getData());
        return "home";
    }
}
