package com.self.cloudmall.wms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableRabbit
@EnableFeignClients(basePackages = "com.self.cloudmall.wms.client")
@EnableDiscoveryClient
@MapperScan("com.self.cloudmall.wms.mapper")
@SpringBootApplication
public class CloudWmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudWmsApplication.class, args);
    }

}
