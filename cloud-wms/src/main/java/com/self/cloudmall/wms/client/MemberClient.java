package com.self.cloudmall.wms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("cloud-ums")
public interface MemberClient {
    @GetMapping("/ums/memberreceiveaddress/info/{id}")
    R receiveAddressInfo(@PathVariable("id") Long id);
}
