package com.self.cloudmall.wms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("cloud-oms")
public interface OrderClient {

    @GetMapping(value = "oms/order/status/{orderSn}")
    R getOrderStatus(@PathVariable("orderSn") String  orderSn);

}
