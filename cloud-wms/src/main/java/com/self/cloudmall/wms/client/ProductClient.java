package com.self.cloudmall.wms.client;

import com.self.cloudmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @version v1.0
 * @EnumName: ProductClient
 * @Description:
 * @Author: M10003729
 */
@FeignClient(value = "cloud-pms")
public interface ProductClient {

    @RequestMapping("/pms/skuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);
}
