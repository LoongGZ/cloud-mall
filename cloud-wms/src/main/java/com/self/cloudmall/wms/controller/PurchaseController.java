package com.self.cloudmall.wms.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.self.cloudmall.wms.vo.MergeVo;
import com.self.cloudmall.wms.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import com.self.cloudmall.wms.entity.PurchaseEntity;
import com.self.cloudmall.wms.service.PurchaseService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.common.utils.R;



/**
 * 采购信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@RestController
@RequestMapping("wms/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;


    @PostMapping("/done")
    public R finish(@RequestBody PurchaseDoneVo doneVo){
        if (doneVo.getId() == null || CollectionUtils.isEmpty(doneVo.getItems())){
            return R.error("采购单id或需求id参数不能为空");
        }
        R r = purchaseService.done(doneVo);
        if (!r.isSuccess()){
            return r;
        }
        return R.ok();
    }

    @PostMapping("/received")
    public R received(@RequestBody List<Long> ids){
        if (CollectionUtils.isEmpty(ids)){
            return R.error("采购单id参数不能为空");
        }
        R r = purchaseService.received(ids);
        if (!r.isSuccess()){
            return r;
        }
        return R.ok();
    }

    @RequestMapping("/unreceive/list")
    //@RequiresPermissions("ware:purchase:list")
    public R unreceivelist(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPageUnreceivePurchase(params);

        return R.ok().put("page", page);
    }


    @PostMapping("/merge")
    public R merge(@RequestBody MergeVo mergeVo){
        if (CollectionUtils.isEmpty(mergeVo.getItems())){
            return R.error("需求单id参数不能为空");
        }
        R r = purchaseService.mergePurchase(mergeVo);
        if (!r.isSuccess()){
            return r;
        }
        return R.ok();
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("wms:purchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("wms:purchase:info")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("wms:purchase:save")
    public R save(@RequestBody PurchaseEntity purchase){
        Date date = new Date();
        purchase.setCreateTime(date);
        purchase.setUpdateTime(date);
		purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("wms:purchase:update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("wms:purchase:delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
