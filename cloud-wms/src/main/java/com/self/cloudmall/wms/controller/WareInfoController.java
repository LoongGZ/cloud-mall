package com.self.cloudmall.wms.controller;

import java.util.Arrays;
import java.util.Map;

import com.self.cloudmall.common.dto.ReceAddrFareDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.self.cloudmall.wms.entity.WareInfoEntity;
import com.self.cloudmall.wms.service.WareInfoService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.common.utils.R;



/**
 * 仓库信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@RestController
@RequestMapping("wms/wareinfo")
public class WareInfoController {
    @Autowired
    private WareInfoService wareInfoService;


    @GetMapping("/fare")
    public R getFare(@RequestParam("addrId") Long addrId){
        ReceAddrFareDto receAddrFare = wareInfoService.getFare(addrId);
        return R.ok().setData(receAddrFare);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("wms:wareinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareInfoService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("wms:wareinfo:info")
    public R info(@PathVariable("id") Long id){
		WareInfoEntity wareInfo = wareInfoService.getById(id);

        return R.ok().put("wareInfo", wareInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("wms:wareinfo:save")
    public R save(@RequestBody WareInfoEntity wareInfo){
		wareInfoService.save(wareInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("wms:wareinfo:update")
    public R update(@RequestBody WareInfoEntity wareInfo){
		wareInfoService.updateById(wareInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("wms:wareinfo:delete")
    public R delete(@RequestBody Long[] ids){
		wareInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
