package com.self.cloudmall.wms.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.self.cloudmall.common.dto.WareSkuLockDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import com.self.cloudmall.wms.entity.WareSkuEntity;
import com.self.cloudmall.wms.service.WareSkuService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.common.utils.R;



/**
 * 商品库存
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:25
 */
@RestController
@RequestMapping("wms/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;


    /**
     * 锁定库存
     * @param skuLockDto
     * @return
     */
    @PostMapping("/order/lock")
    public R orderLockStock(@RequestBody WareSkuLockDto skuLockDto){
        return wareSkuService.orderLockStock(skuLockDto);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("wms:waresku:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }

    /**
     *是否有库存
     * @param skuIds
     * @return
     */
    @PostMapping("/hasStock")
    public R<Map<Long,Boolean>> hasStock(@RequestBody List<Long> skuIds){
        if (CollectionUtils.isEmpty(skuIds)){
            return R.error("skuId参数不能为空");
        }
        R ok = R.ok();
        ok.setData(wareSkuService.hasStock(skuIds));
        return ok;
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("wms:waresku:info")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("wms:waresku:save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("wms:waresku:update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("wms:waresku:delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
