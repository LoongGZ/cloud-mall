package com.self.cloudmall.wms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 库存订单锁定表
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@Data
@TableName("wms_ware_order_lock")
public class WareOrderLockEntity implements Serializable {

	private static final long serialVersionUID = 770644930464721805L;
	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * order_sn
	 */
	private String orderSn;

	/**
	 * sku_id
	 */
	private Long skuId;
	/**
	 * sku_name
	 */
	private String skuName;
	/**
	 * 购买个数
	 */
	private Integer skuNum;

	private Long wareId;

	/**
	 * 1:锁定，2：解锁，3：扣减
	 */
	private Integer lockState;

}
