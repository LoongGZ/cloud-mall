package com.self.cloudmall.wms.mapper;

import com.self.cloudmall.wms.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@Mapper
public interface PurchaseDetailMapper extends BaseMapper<PurchaseDetailEntity> {
	
}
