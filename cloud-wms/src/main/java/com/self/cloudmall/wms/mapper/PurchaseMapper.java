package com.self.cloudmall.wms.mapper;

import com.self.cloudmall.wms.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@Mapper
public interface PurchaseMapper extends BaseMapper<PurchaseEntity> {
	
}
