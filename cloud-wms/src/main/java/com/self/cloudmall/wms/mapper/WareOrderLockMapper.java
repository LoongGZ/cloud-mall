package com.self.cloudmall.wms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.self.cloudmall.wms.entity.WareOrderLockEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存订单锁定表
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@Mapper
public interface WareOrderLockMapper extends BaseMapper<WareOrderLockEntity> {
	
}
