package com.self.cloudmall.wms.mapper;

import com.self.cloudmall.wms.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
@Mapper
public interface WareOrderTaskMapper extends BaseMapper<WareOrderTaskEntity> {
	
}
