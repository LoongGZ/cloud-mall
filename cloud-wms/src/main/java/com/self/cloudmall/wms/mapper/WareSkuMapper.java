package com.self.cloudmall.wms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.self.cloudmall.wms.entity.WareSkuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 * 
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:25
 */
@Mapper
public interface WareSkuMapper extends BaseMapper<WareSkuEntity> {

    Long countStock(Long skuId);

    Integer lockStockByWareIdAndSkuId(@Param("skuId") Long skuId, @Param("wareId") Long wareId,
                                      @Param("count") Integer count);

    List<Long> queryExistStockBySkuId(Long skuId);

    Integer unLockStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId,
                        @Param("skuNum") Integer skuNum);
}
