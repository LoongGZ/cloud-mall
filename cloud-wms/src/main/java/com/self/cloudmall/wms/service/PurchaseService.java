package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.entity.PurchaseEntity;
import com.self.cloudmall.wms.vo.MergeVo;
import com.self.cloudmall.wms.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    R mergePurchase(MergeVo mergeVo);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    R received(List<Long> ids);

    R done(PurchaseDoneVo doneVo);
}

