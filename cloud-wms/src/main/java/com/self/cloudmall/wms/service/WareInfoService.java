package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.entity.WareInfoEntity;
import com.self.cloudmall.common.dto.ReceAddrFareDto;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCondition(Map<String, Object> params);

    ReceAddrFareDto getFare(Long addrId);
}

