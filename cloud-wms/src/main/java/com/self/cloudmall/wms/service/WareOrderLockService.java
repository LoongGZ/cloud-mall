package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.wms.entity.WareOrderLockEntity;
import com.self.cloudmall.wms.utils.PageUtils;

import java.util.Map;

/**
 * 库存订单锁定
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
public interface WareOrderLockService extends IService<WareOrderLockEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

