package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

