package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:49
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

