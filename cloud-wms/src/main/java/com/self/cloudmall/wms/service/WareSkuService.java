package com.self.cloudmall.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.self.cloudmall.common.dto.OrderDto;
import com.self.cloudmall.common.dto.StockLockDto;
import com.self.cloudmall.common.dto.WareSkuLockDto;
import com.self.cloudmall.common.utils.R;
import com.self.cloudmall.wms.entity.PurchaseDetailEntity;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.entity.WareSkuEntity;
import com.self.cloudmall.wms.vo.PurchaseItemDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author mac
 * @email mac@gmail.com
 * @date 2020-08-12 18:45:25
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByCondition(Map<String, Object> params);

    boolean addWareSku(PurchaseItemDoneVo item, PurchaseDetailEntity purchaseDetail);

    Map<Long,Boolean> hasStock(List<Long> skuIds);

    R orderLockStock(WareSkuLockDto skuLockDto);

    void unLockStock(List<StockLockDto> lockDtoList) throws Exception;

    void unLockStock(OrderDto orderDto) throws Exception;
}

