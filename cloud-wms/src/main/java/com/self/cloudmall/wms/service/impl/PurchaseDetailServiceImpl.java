package com.self.cloudmall.wms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.utils.Query;

import com.self.cloudmall.wms.mapper.PurchaseDetailMapper;
import com.self.cloudmall.wms.entity.PurchaseDetailEntity;
import com.self.cloudmall.wms.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailMapper, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                new QueryWrapper<PurchaseDetailEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        /// key: this.dataForm.key,
        //          status: this.dataForm.status,
        //          wareId
        QueryWrapper<PurchaseDetailEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<PurchaseDetailEntity> lambda = queryWrapper.lambda();
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)){
            lambda.and(w -> w.eq(PurchaseDetailEntity::getId,key).or()
            .eq(PurchaseDetailEntity::getPurchaseId,key));
        }
        String status = (String) params.get("status");
        if (StringUtils.isNotEmpty(status)){
            lambda.eq(PurchaseDetailEntity::getStatus,status);
        }
        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotEmpty(wareId)){
            lambda.eq(PurchaseDetailEntity::getWareId,wareId);
        }
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}