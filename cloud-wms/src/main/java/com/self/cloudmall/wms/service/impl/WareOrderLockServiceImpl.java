package com.self.cloudmall.wms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.self.cloudmall.wms.entity.WareOrderLockEntity;
import com.self.cloudmall.wms.entity.WareOrderTaskDetailEntity;
import com.self.cloudmall.wms.mapper.WareOrderLockMapper;
import com.self.cloudmall.wms.mapper.WareOrderTaskDetailMapper;
import com.self.cloudmall.wms.service.WareOrderLockService;
import com.self.cloudmall.wms.service.WareOrderTaskDetailService;
import com.self.cloudmall.wms.utils.PageUtils;
import com.self.cloudmall.wms.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("wareOrderLockService")
public class WareOrderLockServiceImpl extends ServiceImpl<WareOrderLockMapper, WareOrderLockEntity> implements WareOrderLockService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareOrderLockEntity> page = this.page(
                new Query<WareOrderLockEntity>().getPage(params),
                new QueryWrapper<WareOrderLockEntity>()
        );

        return new PageUtils(page);
    }

}