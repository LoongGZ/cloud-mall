package com.self.cloudmall.wms.vo;

import lombok.Data;

import java.util.List;

/**
 * @version v1.0
 * @ClassName: MergeVo
 * @Description:
 */
@Data
public class MergeVo {
    private Long purchaseId;
    private List<Long> items;
}
