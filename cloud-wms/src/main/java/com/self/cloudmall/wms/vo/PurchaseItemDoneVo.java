package com.self.cloudmall.wms.vo;

import lombok.Data;

@Data
public class PurchaseItemDoneVo {
    //采购详情id
    private Long itemId;
    private Integer status;
    private Integer realStock;
    private String reason;
}
